__author__ = 'Olivier Van Cutsem'

import numpy


class statDistribution:
    modelsList = ['NORM', 'UNIF']

    def __init__(self, t, param):

        self.statModel = t
        if t not in self.modelsList:
            self.statModel = self.modelsList[0]

        self._param = param

    @property
    def model(self):
        return self.statModel

    @property
    def param(self):
        return self._param

    def generateValue(self):
        if self.statModel == self.modelsList[0]:
            return self._param[0] + self._param[1] * numpy.random.randn()
        elif self.statModel == self.modelsList[1]:
            return self._param[0] + (self._param[1] - self._param[0]) * numpy.random.random_sample()

    @property
    def equivalent_quantity(self):
        if self.statModel == self.modelsList[0]:
            return self._param[0]
        elif self.statModel == self.modelsList[1]:
            return (self._param[0] + self._param[1])/2

    def __le__(self, stat_distr):
        return self.equivalent_quantity < stat_distr.equivalent_quantity


