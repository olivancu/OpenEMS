
import numpy
from stat_distr import statDistribution

VE_PARAM_LP_ACTIVITY_LIST = 'lp_activity_list'
VE_PARAM_LP_ACTIVITY_PERIOD = 'lp_activity_period'

VE_PARAM_LP_ACTIVITY_ATOM_INSTANT_TYPE_IS_START = 'time_instant_is_starting_time'

VE_PARAM_LP_ACTIVITY_ATOM_INSTANT_DISTR_TYPE = 't_instant_stat_model_type'
VE_PARAM_LP_ACTIVITY_ATOM_INSTANT_DISTR_PARAM = 't_instant_stat_model_param'
VE_PARAM_LP_ACTIVITY_ATOM_DUR_DISTR_TYPE = 'dur_stat_model_type'
VE_PARAM_LP_ACTIVITY_ATOM_DUR_DISTR_PARAM = 'dur_stat_model_param'

VE_PARAM_LP_ACTIVITY_ATOM_PROBA = 'proba'
VE_PARAM_LP_ACTIVITY_ATOM_PARAM = 'param'
VE_PARAM_LP_ACTIVITY_ATOM_START_STATE = 'start_state'
VE_PARAM_LP_ACTIVITY_ATOM_STOP_STATE = 'stop_state'


class LPActivities(object):
    """
    Defines a set of load profile activities
    """

    def __init__(self, act_descr):

        assert(v in act_descr for v in [VE_PARAM_LP_ACTIVITY_LIST, VE_PARAM_LP_ACTIVITY_PERIOD])
        self.__period = act_descr[VE_PARAM_LP_ACTIVITY_PERIOD]

        list_act = act_descr[VE_PARAM_LP_ACTIVITY_LIST]
        self.__list_act = []

        for a in list_act:
            assert(v in a for v in [VE_PARAM_LP_ACTIVITY_ATOM_INSTANT_TYPE_IS_START,
                                    VE_PARAM_LP_ACTIVITY_ATOM_INSTANT_DISTR_TYPE, VE_PARAM_LP_ACTIVITY_ATOM_INSTANT_DISTR_PARAM,
                                    VE_PARAM_LP_ACTIVITY_ATOM_DUR_DISTR_TYPE, VE_PARAM_LP_ACTIVITY_ATOM_DUR_DISTR_PARAM,
                                    VE_PARAM_LP_ACTIVITY_ATOM_START_STATE, VE_PARAM_LP_ACTIVITY_ATOM_STOP_STATE])

            bounds = (a[VE_PARAM_LP_ACTIVITY_ATOM_START_STATE], a[VE_PARAM_LP_ACTIVITY_ATOM_STOP_STATE])
            t_instant_dist = (a[VE_PARAM_LP_ACTIVITY_ATOM_INSTANT_DISTR_TYPE], a[VE_PARAM_LP_ACTIVITY_ATOM_INSTANT_DISTR_PARAM])
            t_instant_is_start = a[VE_PARAM_LP_ACTIVITY_ATOM_INSTANT_TYPE_IS_START]
            dur = (a[VE_PARAM_LP_ACTIVITY_ATOM_DUR_DISTR_TYPE], a[VE_PARAM_LP_ACTIVITY_ATOM_DUR_DISTR_PARAM])
            prob = 1
            param = None
            if VE_PARAM_LP_ACTIVITY_ATOM_PROBA in a:
                prob = a[VE_PARAM_LP_ACTIVITY_ATOM_PROBA]
            if VE_PARAM_LP_ACTIVITY_ATOM_PARAM in a:
                param = a[VE_PARAM_LP_ACTIVITY_ATOM_PARAM]

            self.__list_act.append(LPAtomActivity(bounds, t_instant_dist, dur, t_instant_is_start, prob, param))

    def generate_period_activities(self):

        selected_activities = []

        for a in self.__list_act:
            if a.proba < numpy.random.random_sample():  # pass this activity?
                continue

            start_time = max(a.time_instant.generateValue(), 0)
            duration = max(a.duration.generateValue(), 0)
            if not a.type_time_instant_is_start:  # the encoded value is the ending time !
                start_time = max(start_time - duration, 0)

            a_descr = {'start_time': start_time,
                       'dur': duration,
                       'param': a.param,
                       'starting_state': a.starting_state,
                       'ending_state': a.ending_state}

            selected_activities.append(a_descr)

        return sorted(selected_activities, key=lambda x: x['start_time'])

    def get(self, i):
        return self.__list_act[i]

    @property
    def size(self):
        return len(self.__list_act)

    @property
    def period(self):
        return self.__period


class LPAtomActivity(object):
    """
    Defines a load profile activity
    """

    def __init__(self, state_bounds, time_instant_descr, duration_descr, t_instant_is_start=True, proba=1, param=None):

        # Start and stop state
        self.__state_bound = state_bounds

        # Starting time
        t_instant_type_distr, t_instant_param_distr = time_instant_descr
        self.__t_instant_distribution = statDistribution(t_instant_type_distr, t_instant_param_distr)
        self.__t_instant_type_is_start = t_instant_is_start

        # Duration
        duration_type_distr, duration_param_distr = duration_descr
        self.__duration_distribution = statDistribution(duration_type_distr, duration_param_distr)

        # Activity probability
        self.__probability = proba

        # Activity parameter
        self.__param = param

    @property
    def starting_state(self):
        return self.__state_bound[0]

    @property
    def ending_state(self):
        return self.__state_bound[1]

    @property
    def mean_time_instant(self):
        if self.__t_instant_type_is_start:
            return self.__t_instant_distribution.equivalent_quantity
        else:  # Ending time is stored
            return self.__t_instant_distribution.equivalent_quantity - self.mean_duration

    @property
    def mean_duration(self):
        return self.__duration_distribution.equivalent_quantity

    @property
    def time_instant(self):
        return self.__t_instant_distribution

    @property
    def type_time_instant_is_start(self):
        return self.__t_instant_type_is_start

    @property
    def duration(self):
        return self.__duration_distribution

    @property
    def proba(self):
        return self.__probability

    @property
    def param(self):
        return self.__param
