__author__ = 'Olivier Van Cutsem'

from stat_distr import statDistribution
from mode_sequence import LPSequence

# States
LOAD_PROFILE_STATE_DEFAULT = 'lp_states_default'
LOAD_PROFILE_STATE_LIST = 'lp_states_list'

# Modes
LOAD_PROFILE_MODE_MIN_POWER = 'p_min'
LOAD_PROFILE_MODE_MAX_POWER = 'p_max'
LOAD_PROFILE_MODE_POWER_DISTR_TYPE = 'type_power_distribution'
LOAD_PROFILE_MODE_POWER_DISTR_PARAM = 'param_power_distribution'


class LoadProfile(object):
    """
    Define the generic structure of a load profile
    """

    def __init__(self, modes_dict, state_dict, state_trans_dict=None):
        """
        :param modes_dict: a formatted dictionary describing the possible power modes of the load
        :param state_dict: a formatted dictionary describing the possible power states of the load
        """
        # The sequences in steady states

        # The load states
        assert (type(state_dict) is dict and LOAD_PROFILE_STATE_LIST in state_dict.keys())
        list_states = state_dict[LOAD_PROFILE_STATE_LIST]
        if LOAD_PROFILE_STATE_DEFAULT not in state_dict:
            state_dict[LOAD_PROFILE_STATE_DEFAULT] = list_states[0]

        self.__states = LPStates(list_states, state_dict[LOAD_PROFILE_STATE_DEFAULT])

        # The list of modes describes the possible power states
        self.__modes = list()

        assert(type(modes_dict) is dict)
        for name, descr in modes_dict.items():
            assert(bd_key in descr for bd_key in [LOAD_PROFILE_MODE_MIN_POWER, LOAD_PROFILE_MODE_MAX_POWER, LOAD_PROFILE_MODE_POWER_DISTR_TYPE, LOAD_PROFILE_MODE_POWER_DISTR_PARAM])
            min_p = descr[LOAD_PROFILE_MODE_MIN_POWER]
            max_p = descr[LOAD_PROFILE_MODE_MAX_POWER]
            type_power_distr = descr[LOAD_PROFILE_MODE_POWER_DISTR_TYPE]
            param_power_distr = descr[LOAD_PROFILE_MODE_POWER_DISTR_PARAM]
            self.__modes.append(LPMode(name, (min_p, max_p), (type_power_distr, param_power_distr)))

        self.__modes.sort(key=lambda x: (x.min+x.max)/2, reverse=False)

        # The transition between states
        self.__states_trans = dict()
        for s in self.__states.list_states:
            self.__states_trans[s] = dict()

        if state_trans_dict is not None and type(state_trans_dict) == dict:
            for s_s in state_trans_dict.keys():
                for s_e in state_trans_dict[s_s]:
                    self.__states_trans[s_s][s_e] = LPSequence(state_trans_dict[s_s][s_e])

        self.sequences = {}


    @property
    def list_of_states(self):
        return self.__states.list_states

    @property
    def default_state(self):
        return self.__states.default_state

    @property
    def list_of_modes(self):
        return self.__modes

    @property
    def max_p(self):
        return self.__modes[-1].max

    @property
    def min_p(self):
        return self.__modes[0].min

    def state_sequence(self, s, s_e=None):

        # Transition between two modes ?
        if s_e is not None and not(s_e == s):
            return self.state_transition_sequence(s, s_e)

        # Steady states
        if s not in self.sequences.keys():
            return None
        else:
            return self.sequences[s]

    def state_transition_sequence(self, s_s, s_e):
        if s_e in self.__states_trans[s_s].keys():
            return self.__states_trans[s_s][s_e]
        else:
            return None

    def get_mode(self, mode_name):

        for m in self.list_of_modes:
            if m.name == mode_name:
                return m

        return None

class LPStates:
    """
    Represents the states of a load
    """

    def __init__(self, list_state, default=None):

        self.__list_state = list_state

        if default is None:
            self.__default_state = list_state[0]
        else:
            self.__default_state = default

    @property
    def default_state(self):
        return self.__default_state

    @property
    def list_states(self):
        return self.__list_state


class LPMode:
    """
    Represents a mode of a load
    """

    def __init__(self, name, min_max, power_distr):
        """
        :param name:
        :param min_max:
        :param power_distr:
        """

        self.__name = name
        self.__min, self.__max = min_max

        type_distr, param_distr = power_distr
        self.__power_distribution = statDistribution(type_distr, param_distr)

    @property
    def min(self):
        return self.__min

    @property
    def max(self):
        return self.__max

    @property
    def name(self):
        return self.__name

    @property
    def bounds(self):
        return [self.__min, self.__max]

    @property
    def power_distribution(self):
        return self.__power_distribution

    def get_dim_value(self, coeff):
        return coeff * self.max
