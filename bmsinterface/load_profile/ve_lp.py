
import numpy
from lp import *
from mode_sequence import LPSequence, LPSequenceStochastique
from activity import *
import json

# DB-keys
VE_PARAM_LP_MODEL = 'lp_model'

VE_PARAM_LP_ACTIVITY = 'lp_activity'

VE_PARAM_LP_STATES = 'lp_states'
VE_PARAM_LP_STATES_DEFAULT = 'lp_state_default'
VE_PARAM_LP_STATES_LIST = 'lp_state_list'

VE_PARAM_LP_MODES = 'lp_modes'

VE_PARAM_LP_SEQUENCES = 'lp_seq'

VE_PARAM_LP_TRANSITION = 'lp_state_trans'

# Sequences
LOAD_PROFILE_SEQ_LIST = 'sequence_list'
LOAD_PROFILE_SEQ_TRANSITION = 'sequence_transitions'

# additionnal parameters
LOAD_PROFILE_ADDITIONNAL_PARAM = 'additionnal_parameters'

# Scheduling constraints
VE_PARAM_LP_SCHED_CONSR_MIN_START = 'scheduling_constraints_min_start'
VE_PARAM_LP_SCHED_CONSR_MAX_STOP = 'scheduling_constraints_max_stop'


class LPUserDriven(LoadProfile):
    """
    Defines the load profile structure of a user-driven load
    """
    def __init__(self, modes, states, state_seq_descr, act_descr, state_trans=None):
        super(LPUserDriven, self).__init__(modes, states, state_trans)

        # bug ..
        self.__sequences = self.sequences

        # List of sequences for each state
        for state, seq_descr in state_seq_descr.items():
            assert(v in seq_descr for v in [LOAD_PROFILE_SEQ_LIST, LOAD_PROFILE_SEQ_TRANSITION])
            seq_list = seq_descr[LOAD_PROFILE_SEQ_LIST]
            seq_trans = seq_descr[LOAD_PROFILE_SEQ_TRANSITION]
            self.__sequences[state] = LPSequenceStochastique(seq_list, seq_trans)

        # List of activities
        self.__activities = LPActivities(act_descr)

    def get_activities(self):
        return self.__activities


class LPShift(LoadProfile):
    """
    Defines the load profile structure of a user-driven load
    """
    def __init__(self, modes, states, state_seq_descr, schedule_constr, state_trans=None):
        super(LPShift, self).__init__(modes, states, state_trans)

        # List of sequences for each state
        self.__sequences = self.sequences
        for state, seq_descr in state_seq_descr.items():
            assert (v in seq_descr for v in [LOAD_PROFILE_SEQ_LIST])
            seq_list = seq_descr[LOAD_PROFILE_SEQ_LIST]
            self.__sequences[state] = LPSequence(seq_list)

        # scheduling constrainte
        self.__min_starting_time, self.__max_ending_time = schedule_constr

    def get_max_starting_time(self, state):
        assert(state in self.__sequences.keys())

        return self.__max_ending_time - self.duration(state)

    @property
    def min_starting_time(self):
        return self.__min_starting_time

    @property
    def max_ending_time(self):
        return self.__max_ending_time

    def duration(self, state):
        return self.__sequences[state].sequence_duration()

    def get_iter(self, state):
        return iter_seq(self.__sequences[state])


def iter_seq(seq):

    for i in range(0, len(seq)):
        yield seq.get(i)

def instanciate_lp_from_db(lp_param_db):

    # Modes
    type_load_seq_deterministic = True
    mode_descr = json.loads(lp_param_db.pop(VE_PARAM_LP_MODES, None))

    # States
    state_descr = {LOAD_PROFILE_STATE_LIST: json.loads(lp_param_db.pop(LOAD_PROFILE_STATE_LIST, None)),
                   LOAD_PROFILE_STATE_DEFAULT: lp_param_db.pop(LOAD_PROFILE_STATE_DEFAULT, None)}

    # State transitions
    state_trans_descr = None
    if VE_PARAM_LP_TRANSITION in lp_param_db.keys():
        val = json.loads(lp_param_db.pop(VE_PARAM_LP_TRANSITION, None))
        if val != {}:
            state_trans_descr = val

    # Sequences
    seq_descr = json.loads(lp_param_db.pop(VE_PARAM_LP_SEQUENCES, None))

    first_state = seq_descr.keys()[0]
    if LOAD_PROFILE_SEQ_TRANSITION in seq_descr[first_state]:
        type_load_seq_deterministic = False

    # Instanciate the object
    lp = None
    if type_load_seq_deterministic:
        # Scheduling constraints:
        schedule_constr = (lp_param_db.pop(VE_PARAM_LP_SCHED_CONSR_MIN_START, None),
                           lp_param_db.pop(VE_PARAM_LP_SCHED_CONSR_MAX_STOP, None))

        lp = LPShift(modes=mode_descr, states=state_descr, state_seq_descr=seq_descr,
                     schedule_constr=schedule_constr,
                     state_trans=state_trans_descr)  # load profile object creation
    else:
        # Activities:
        act_descr = {VE_PARAM_LP_ACTIVITY_LIST: json.loads(lp_param_db.pop(VE_PARAM_LP_ACTIVITY_LIST), None),
                     VE_PARAM_LP_ACTIVITY_PERIOD: lp_param_db.pop(VE_PARAM_LP_ACTIVITY_PERIOD, None)}

        lp = LPUserDriven(modes=mode_descr, states=state_descr, state_seq_descr=seq_descr,
                          act_descr=act_descr,
                          state_trans=state_trans_descr)  # load profile object creation

    return lp