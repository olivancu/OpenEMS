__author__ = 'Olivier Van Cutsem'

from Queue import Queue
from threading import Thread
from bmsinterface.conf_ems import *
import bmsinterface.auxiliary.websocket as websocket
import bmsinterface.auxiliary.requests.api as requests
import json


class BMSInterface(Thread):
    """
    Description and how to use it
    """

    def __init__(self, out_q):
        """
        Initialize BMSInterface object.
        Launch a thread to listen to BMS bubble messages through a websocket
        Initialize the dictionary to link sensors and loads
        Filter the incoming messages
        """
        Thread.__init__(self)
        self.sensors_to_loads_list = {}
        self.out_stream = out_q

    def run(self):
        """
        To be executed during thread process
        """
        websocket.enableTrace(False) # True if you need debug info
        print("Attempt to connect ...")
        ws = websocket.create_connection("ws://{0}:{1}/websocket".format(RT_SERVER_IP,RT_SERVER_PORT))

        ws.send("Hello World")  # not recognized
        ws.send("ECHO testing reply")  # ECHO + space + command verifies the loop
        print("Sent")

        print("Received: {0}".format(ws.recv()))

        # Register interest to all room (-1) otherwise give room ID list to register to events
        regmsg = {"PUB_ID":-1, "MSG_TYPE":"WEB_MSG", "PAYLOAD": {"MSG_CLASS":"REG_PUBLISHER"}}
        ws.send(json.dumps(regmsg))
        while True:
            result = ws.recv()
            print("Received: {0}".format(result))
            self.decode_bms_msg(result)

    def decode_bms_msg(self, in_msg):
        """Interpret the bubble message coming from the BMS and update the appropriate structures"""
        if in_msg[0] != '{':
            return

        json_obj = json.loads(in_msg)

        if 'PAYLOAD' in json_obj:
            json_payload_obj = json_obj['PAYLOAD']

            #TODO: adapt for virtual loads

            if ('MSG_CLASS' in json_payload_obj and 'QUANTITY' in json_payload_obj and json_payload_obj['QUANTITY'] == 'P') or (0):
                print('New value of P: {0} from sensor {1}'.format(json_payload_obj['VALUE'], json_obj['ID']))
                self.bubble_messages(json_payload_obj['VALUE'], json_obj['ID'])

    def bubble_messages(self, val, sensor_id):
        """Send a bubble message up to the CMS block"""
        if sensor_id not in self.sensors_to_loads_list:
            load_id = get_bms_load_id(sensor_id)

            if load_id >= 0:
                load_info = get_bms_load_info(load_id)
                if load_info != "":
                    self.out_stream.put({'ID': load_id, 'MSG_TYPE': INIT_ENTITY_INTERFACE_MSG, 'PAYLOAD': load_info})
                else:
                    return
            else:
                return

            self.sensors_to_loads_list[sensor_id] = load_id

        self.out_stream.put({'ID': self.sensors_to_loads_list[sensor_id], 'MSG_TYPE': NEW_VALUE_INTERFACE_MSG, 'PAYLOAD': val})


def get_bms_load_id(sensor_id):
    """Retrieve the id of the load to which the sensor_id is connected"""
    sensor_info = get_bms_info('sensor', sensor_id)

    if 'fields' not in sensor_info[0]:
        return -1

    sensor_info = sensor_info[0]['fields']
    # Retrieve the load_id from the info

    print(sensor_info['load'])

    if 'load' in sensor_info:
        return sensor_info['load']
    else:
        return -1

def get_bms_load_info(load_id):
    """Retrieve the load information of the load_id"""
    load_info = get_bms_info('load', load_id)
    # Retrieve the load_id from the info

    if 'fields' not in load_info[0]:
        return -1

    load_info = load_info[0]['fields']

    #  TO DO: format the message to send up
    if 'additional_parameters' in load_info:
        return load_info['additional_parameters'] + '{"test": 4}'
    else:
        return ""


def get_bms_info(resource_type='room', resource_id=1):
        """Returns the info for the given ID according to the docs in openBMS"""
        geturl = 'http://{0}:{1}/API/{2}/{3}'.format(OPENBMS_IP, OPENBMS_PORT, resource_type, resource_id)
        r = requests.get(geturl)

        # always check the return status code!
        if r.status_code == 200:  # Otherwise it means openBMS doesnt work
            request_result = r.json()
            return request_result
        elif r.status_code == 404 and r.content:  # Server replied but not found the midid
            print('Failed to find in openBMS the {0}: {1}'.format(resource_type, resource_id))
        else:
            print('Failed to connect to {0}:{1}, is openBMS running?'.format(OPENBMS_IP,OPENBMS_PORT))
