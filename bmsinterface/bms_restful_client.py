__author__ = 'Olivier Van Cutsem'

from ems_config import *
from bmsinterface.bms_interface_config import *
import bmsinterface.auxiliary.requests.api as requests
from bmsinterface.load_profile.ve_lp import *
from building_data_management.category_management.category_config import *
import sys
import json


def get_bms_info(resource_type, resource_id):
    """Returns the info for the given ID according to the docs in openBMS

    @IN:
    resource_type: the type of required resource (unit info, room loads, etc)
    resource_id: the ID corresponding to the desired resource

    return: the JSON format of the request result
    """
    geturl = 'http://{0}:{1}/API/{2}/{3}'.format(OPENBMS_IP, OPENBMS_PORT, resource_type, resource_id)
    r = requests.get(geturl)

    # always check the return status code!
    if r.status_code == 200:  # Otherwise it means openBMS doesnt work
        request_result = r.json()
        return request_result
    elif r.status_code == 404 and r.content:  # Server replied but not found the midid
        print('Failed to find in openBMS the {0}: {1}'.format(resource_type, resource_id))
        return None
    elif r.status_code == 500:  # Server replied but not found the midid
        print('{0}:{1}: Internal error'.format(OPENBMS_IP,OPENBMS_PORT))
        return None
    else:
        print('Failed to connect to {0}:{1}, is openBMS running?'.format(OPENBMS_IP,OPENBMS_PORT))
        return None

### UNIT INFO ###


def get_unit_info(unit_id):
    """
    Fetch the BMS for the unit info of a specified unit
    :param unit_id: the unit whose rooms info should be known
    :return: the formatted data
    """

    ### Make the BMS request
    res_type = 'unit'
    res_id = unit_id

    ret_bms = get_bms_info(res_type, res_id)

    ### Process the request for answer formatting
    data = ret_bms[0]['fields']

    ret = {'LAT': data['latitude'], 'LONG': data['longitude'], 'NAME': data['name']}

    return ret

def get_rooms_in_unit(unit_id):
    """
    Fetch the BMS for the rooms info of a specified unit
    :param unit_id: the unit whose rooms info should be known
    :return: the rooms info contained in the specified unit
    """

    ### Make the BMS request
    res_type = 'unit_rooms'
    res_id = unit_id

    ret_bms = get_bms_info(res_type, res_id)

    ### Process the request for answer formatting
    ret = dict()

    for room in ret_bms:
        id = room['pk']
        data = room['fields']

        # Room nature: human space, energy storage space, or environment ?

        room_nature = EMS_ROOM_NATURE_HUMAN_SPACE
        if 'nature' in data.keys():
            room_nature = EMS_ROOM_NATURE_FROM_DB[data['nature']]

        room_type = EMS_ROOM_TYPE_REGULAR_ROOM
        if 'nature' in data.keys():
            room_type_id = data['type']
            if room_type_id in EMS_ROOM_TYPE_FROM_DB.keys():
                room_type = EMS_ROOM_TYPE_FROM_DB[room_type_id]
            else:
                room_type = EMS_ROOM_TYPE_REGULAR_ROOM

        ret[id] = {DB_KEY_ROOM_NAME: data['name'],
                   DB_KEY_ROOM_SURFACE: data['surface'],
                   DB_KEY_ROOM_NATURE: room_nature,
                   DB_KEY_ROOM_TYPE: room_type,
                   DB_KEY_ROOM_VOLUME: data['volume']}

    return ret

def get_interface_geometry(unit_id):
    """
    Fetch the BMS for the room interfaces info of a specified unit
    :param unit_id: the unit whose room interfaces info should be known
    :return: the room interfaces info contained in the specified unit
    """

    ### Make the BMS request
    res_type = 'unit_room_interfaces'
    res_id = unit_id

    ret_bms = get_bms_info(res_type, res_id)

    ### Process the request for answer formatting
    ret = dict()

    for i in ret_bms:
        data = i['fields']
        room_x = data['room_x']
        room_y = data['room_y']

        # Construct EMS-readable interface data
        interf_data = {DB_KEY_INTERFACE_THERMAL_PARAM: json.loads(data['rc_model']),
                       DB_KEY_INTERFACE_TYPE_KEY: 'interface_'+data['interface_type'],
                       DB_KEY_INTERFACE_SURFACE: data['surface'],
                       DB_KEY_INTERFACE_THICKNESS: data['thickness'],
                       DB_KEY_INTERFACE_OPENING_ACTUATOR: None}

        # If the room haven't been scanned yet, initialized the dictionaries
        if room_x not in ret.keys():
            ret[room_x] = dict()
        if room_y not in ret.keys():
            ret[room_y] = dict()

        # If the neighboring room hasn't been scanned yet, initialized the dictionaries to the corresponding dict
        if room_y not in ret[room_x].keys():
            ret[room_x][room_y] = []
        if room_x not in ret[room_y].keys():
            ret[room_y][room_x] = []

        ret[room_x][room_y].append(interf_data)
        ret[room_y][room_x].append(interf_data)

    return ret


def get_loads_data(unit_id):

    ### Make the BMS request

    res_type = 'unit_loads'
    res_id = unit_id

    ret_bms = get_bms_info(res_type, res_id)

    ### Process the request for answer formatting

    ret = dict()

    for load in ret_bms:
        id = load['pk']
        data = load['fields']

        # Load profile object
        try:
            lp_obj = instanciate_lp_from_db(load['fields'])
        except KeyError:
            print("KeyError for {0}: lp is None".format(id))
            lp_obj = None

        # EMS controllability
        if 'ems_type' in data:
            type_load = data['ems_type']
        else:
            type_load = EMS_CATEGORY_ENTITY_LOAD_USER_DRIVEN

        # Device type
        if 'device' in data:
            type_device = data['device']
        else:
            type_device = None

        # Additional model param ?
        if 'additional_parameters' in data and data['additional_parameters'] is not None:
            add_param = json.loads(data['additional_parameters'])
        else:
            add_param = None

        current_value = get_load_last_value(id)

        ret[id] = {DB_KEY_LOAD_NAME: data['name'],
                   DB_KEY_LOAD_LP: lp_obj,
                   DB_KEY_LOAD_CONTROL_TYPE: type_load,
                   DB_KEY_LOAD_DEVICE_TYPE: type_device,
                   DB_KEY_LOAD_CURRENT_VALUE: current_value,
                   DB_KEY_LOAD_MODEL_PARAM: add_param}

    return ret


def get_storage_data(unit_id):

    ret = dict()

    # First, get the Batteries and ignore the ones linked to the PHEVs
    ### Make the BMS request
    res_type = 'unit_storage'
    res_id = unit_id

    ret_bms = get_bms_info(res_type, res_id)

    for stor in ret_bms:
        id = stor['pk']
        data = stor['fields']

        capa = data['nominal_capacity']
        eff = data['average_efficiency']
        current_cycle = data['currect_cycles']
        total_cycles = data['life_cycles']
        model_param = data['additional_parameters']

        max_soc = data['max_soc']
        min_soc = data['min_soc']

        current_soc = data['state_of_charge']
        current_p = get_storage_last_value(id)

        ret[id] = {DB_KEY_STORAGE_TYPE: EMS_CATEGORY_ENTITY_STORAGE_FIXED,
                   DB_KEY_STORAGE_NAME: data['name'],
                   DB_KEY_STORAGE_CAPA: capa,
                   DB_KEY_STORAGE_LIFE: total_cycles,
                   DB_KEY_STORAGE_EFFICIENCY: eff,
                   DB_KEY_STORAGE_MODEL: model_param,
                   DB_KEY_STORAGE_MIN_SOC: min_soc,
                   DB_KEY_STORAGE_MAX_SOC: max_soc,
                   DB_KEY_STORAGE_CURRENT_SOC: current_soc,
                   DB_KEY_STORAGE_CURRENT_POWER: current_p,
                   DB_KEY_STORAGE_CURRENT_CYCLE: current_cycle}


    ############################
    # Then, get the PHEVs to add additional info for each storage system

    ### Make the BMS request
    res_type = 'unit_phevs'
    res_id = unit_id

    ret_bms = get_bms_info(res_type, res_id)

    ### Process the request for answer formatting

    for phev in ret_bms:
        id = phev['pk']
        data = phev['fields']

        linked_batt_id = data['battery']
        ret[linked_batt_id][DB_KEY_PHEV_ID] = id
        ret[linked_batt_id][DB_KEY_STORAGE_TYPE] = EMS_CATEGORY_ENTITY_STORAGE_PHEV
        ret[linked_batt_id][DB_KEY_PHEV_SCHEDULE_ARRIVE_TIME] = (data['schedule_constraints']['scheduling_constraints_min_start_mean'], data['schedule_constraints']['scheduling_constraints_min_start_std'])
        ret[linked_batt_id][DB_KEY_PHEV_SCHEDULE_LEAVE_TIME] = (data['schedule_constraints']['scheduling_constraints_max_stop_mean'], data['schedule_constraints']['scheduling_constraints_max_stop_std'])
        ret[linked_batt_id][DB_KEY_PHEV_ALLOW_DISCHARGE] = data['allow_discharge']

    return ret

def get_generation_data(unit_id):

    ### Make the BMS request
    res_type = 'unit_generators'
    res_id = unit_id

    ret_bms = get_bms_info(res_type, res_id)

    ### Process the request for answer formatting

    ret = dict()

    for gen in ret_bms:
        id = gen['pk']
        data = gen['fields']

        type = data['type']
        power_nom = data['nominal_power']
        current_power = data['current_production']
        model_param = data['additional_parameters']

        ret[id] = {DB_KEY_GENERATION_NAME: data['name'],
                   DB_KEY_GENERATION_TYPE: type,
                   DB_KEY_GENERATION_NOM_POWER: power_nom,
                   DB_KEY_GENERATION_CURRENT_POWER: current_power,
                   DB_KEY_GENERATION_MODEL_PARAM: model_param
                   }

    return ret

### ROOM INFO ###
def get_ambient_sensors_in_room(room_id):
    """
    Fetch the BMS for the room info of a specified room
    :param unit_id: the unit whose rooms info should be known
    :return: the rooms info contained in the specified unit
    """
    res_type = 'room_sensors'
    res_id = room_id

    ret_bms = get_bms_info(res_type, res_id)

    ### Process the request for answer formatting
    sensor_of_interest = EMS_LIST_COMFORT_QUANTITIES
    ret = dict()

    for sensor in ret_bms:
        data = sensor['fields']
        id = sensor['pk']

        type_meas = DB_KEY_EMS_SENSORS_CONVERSION[data['measure']]

        if type_meas not in sensor_of_interest:  # check only the comfort sensors
            continue

        if EMS_MODE == EMS_MODE_VE_SIMULATION:
            current_value = None
        else:
            current_value = get_sensor_last_value(id)

        ret[id] = {DB_KEY_COMFORT_TYPE: type_meas,
                   DB_KEY_COMFORT_VALUE: current_value}

    return ret

def get_energy_sensors_in_room(room_id):

    res_type = 'room_sensors'
    res_id = room_id

    ret_bms = get_bms_info(res_type, res_id)

    ### Process the request for answer formatting
    sensor_of_interest = EMS_LIST_ENERGY_RELATED_QUANTITIES
    ret = dict()

    for sensor in ret_bms:
        data = sensor['fields']
        id = sensor['pk']

        type_meas = DB_KEY_EMS_SENSORS_CONVERSION[data['measure']]

        if type_meas not in sensor_of_interest:  # check only the energy-related sensors
            continue

        if EMS_MODE == EMS_MODE_VE_SIMULATION:
            current_value = None
        else:
            current_value = get_sensor_last_value(id)

        type_entity = None
        entity_related_id = None
        if data['load'] is not None:
            type_entity = EMS_CATEGORY_ENTITY_LOAD
            entity_related_id = data['load']
        elif data['storage'] is not None:
            type_entity = EMS_CATEGORY_ENTITY_STORAGE
            entity_related_id = data['storage']
        elif data['generator'] is not None:
            type_entity = EMS_CATEGORY_ENTITY_GENERATION
            entity_related_id = data['generator']

        ret[id] = {DB_KEY_ENERGY_MEASURE_TYPE: type_meas,
                   DB_KEY_ENERGY_MEASURE_VALUE: current_value,
                   DB_KEY_ENERGY_ENTITY_RELATED_TYPE: type_entity,
                   DB_KEY_ENERGY_ENTITY_RELATED_ID: entity_related_id}

    return ret


def get_thermal_loads_in_room(room_id):

    res_type = 'room_loads'
    res_id = room_id

    ret_bms = get_bms_info(res_type, res_id)

    ### Process the request for answer formatting
    ret = list()

    for l_data in ret_bms:

        # Get control type
        data_field = l_data['fields']
        l_id = l_data['pk']

        if data_field['ems_type'] == EMS_CATEGORY_ENTITY_LOAD_THERM:
            ret.append(l_id)

    return ret

def get_actuators_in_room(room_id):
    res_type = 'room_actuators'
    res_id = room_id

    ret_bms = get_bms_info(res_type, res_id)

    ### Process the request for answer formatting
    ret = dict()

    for actuator in ret_bms:
        data = actuator['fields']
        id = actuator['pk']

        type_entity = None
        entity_related_id = None
        if data['load'] is not None:
            type_entity = EMS_CATEGORY_ENTITY_LOAD
            entity_related_id = data['load']
        elif data['storage'] is not None:
            type_entity = EMS_CATEGORY_ENTITY_STORAGE
            entity_related_id = data['storage']
        elif data['generator'] is not None:
            type_entity = EMS_CATEGORY_ENTITY_GENERATION
            entity_related_id = data['generator']

        # Load profile ?
        add_param = None
        lp_act = None
        if data['additional_parameters'] is not None:
            add_param = json.loads(data['additional_parameters'])

            if VE_PARAM_LP_ACTIVITY in add_param.keys():
                lp_act = LPActivities(add_param.pop(VE_PARAM_LP_ACTIVITY, None))

        ret[id] = {DB_KEY_ENERGY_ENTITY_RELATED_TYPE: type_entity,
                   DB_KEY_ENERGY_ENTITY_RELATED_ID: entity_related_id,
                   DB_KEY_ACTUATOR_ACTION_TYPE: data['action'],
                   DB_KEY_ACTUATOR_NAME: data['name'],
                   DB_KEY_ACTUATOR_LP: lp_act,
                   DB_KEY_ACTUATOR_ADD_PARAM: add_param}

    return ret

def get_room_constraints(room_id):
    """
    TODO
    :param room_id:
    :return:
    """

    res_type = 'room_constraints'
    res_id = room_id

    ret_bms = get_bms_info(res_type, res_id)

    ### Process the request for answer formatting

    ret = {}

    for constr in ret_bms:
        data = constr['fields']
        quantity_type = data['quantity_type']
        ems_comf_type = DB_KEY_EMS_SENSORS_CONVERSION[quantity_type]

        ret[ems_comf_type] = {}
        ret[ems_comf_type][DB_KEY_COMFORT_CONSTRAINTS] = json.loads(data['constraints_timeseries'])  # constraints
        ret[ems_comf_type][DB_KEY_COMFORT_REFERENCE_VALUE] = data['ideal_quantity']  # ref value

    return ret

### INSTANTANEOUS INFO ###
def get_sensor_last_value(sensor_id):

    res_type = 'last/quantity'
    res_id = sensor_id

    ret_bms = get_bms_info(res_type, res_id)

    ### Process the request for answer formatting
    if ret_bms is None:
        return None

    key_type = list(ret_bms.keys())
    key_type = key_type[0]
    time_value = ret_bms[key_type]  # the answer is [time, value]
    return time_value[1]

def get_load_last_value(load_id):

    res_type = 'last/load'
    res_id = load_id

    ret_bms = get_bms_info(res_type, res_id)

    ### Process the request for answer formatting
    key_type = list(ret_bms.keys())
    key_type = key_type[0]
    time_value = ret_bms[key_type]  # the answer is [time, value]
    return time_value[1]

def get_actuator_status(actuator_id):

    res_type = 'status/actuator'
    res_id = actuator_id

    ret_bms = get_bms_info(res_type, res_id)

    ### Process the request for answer formatting
    if 'Status' in ret_bms:
        return ret_bms['Status']
    else:
        return None

def get_storage_last_value(storage_id):

    res_type = 'last/storage'
    res_id = storage_id

    ret_bms = get_bms_info(res_type, res_id)

    ### Process the request for answer formatting
    key_type = list(ret_bms.keys())
    key_type = key_type[0]
    time_value = ret_bms[key_type]  # the answer is [time, value]
    return time_value[1]

### Load, Storage and Generator data ###

def get_energy_entity_sensors(entity_id, type_entity):

    res_type = None
    if type_entity == EMS_CATEGORY_ENTITY_LOAD:
        res_type = 'load_sensors'
    elif type_entity == EMS_CATEGORY_ENTITY_STORAGE:
        res_type = 'storage_sensors'
    elif type_entity == EMS_CATEGORY_ENTITY_GENERATION:
        res_type = 'generation_sensors'

    res_id = entity_id

    ret_bms = get_bms_info(res_type, res_id)

    ### Process the request for answer formatting
    ret = {}

    for sensor_detail in ret_bms:
        measure_ems_translated = DB_KEY_EMS_SENSORS_CONVERSION[sensor_detail["fields"]["measure"]]
        ret[measure_ems_translated] = sensor_detail["pk"]  # measure -> ID

    return ret

### Simulation data ###

def get_simulation_entities(mid_id):
    """
    Return the list of
    :return:
    """

    res_type = 'v_middleware_entities'
    res_id = mid_id

    ret_bms = get_bms_info(res_type, mid_id)

    ### Process the request for answer formatting

    return ret_bms

def get_unit_middleware(unit_id):
    """
    Return the list of middleware IDs that collect the data
    :return:
    """

    res_type = 'unit_middlewares'
    res_id = unit_id

    ret_bms = get_bms_info(res_type, res_id)

    ### Process the request for answer formatting

    ret = []
    for mid_data in ret_bms:
        mid_id = mid_data['pk']
        mid_type = mid_data['fields']['middleware_type']

        if mid_type == 'DB':
            continue
        else:
            ret.append(mid_id)

    return ret

def get_simu_pub_sub(mid_id):
    """
    Return the pair pub/sub for ZMQ connection to the simulator
    :return:
    """

    res_type = 'middleware'
    res_id = mid_id

    ret_bms = get_bms_info(res_type, res_id)

    ### Process the request for answer formatting

    pub = None
    sub = None

    mid_data = ret_bms[0]

    pub = mid_data['fields']['PUB_port']
    sub = mid_data['fields']['REP_port']

    return pub, sub