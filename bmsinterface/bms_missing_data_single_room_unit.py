__author__ = 'Olivier Van Cutsem'

###### FOR UNIT 7 #####

from building_data_management.category_management.category_config import *
from bmsinterface.bms_interface_config import DB_KEY_INTERFACE_WALL_KEY,DB_KEY_INTERFACE_THERMAL_PARAM,DB_KEY_INTERFACE_SURFACE, DB_KEY_INTERFACE_THICKNESS
from ems_config import EMS_OUTSIDE_ROOM_NAME

# This file is use to encode directly the data missing in the BMS #

### Link generation and their sensor

# --- Price signal:

BMS_MISSING_DATA_ENVIRONMENTAL_SIGNAL = {
    'price':
        {'t': [0, 10*3600, 18*3600, 24*3600],
         'v': [0.2, 0.04, 0.4, 0.4]}
}



