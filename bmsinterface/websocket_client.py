__author__ = 'Georgios Lilis'

from conf_ems import *
import bmsinterface.auxiliary.websocket as websocket
import json


print("Start")

if __name__ == "__main__":

    websocket.enableTrace(False) #True if you need debug info
    print("Attempt to connect ...")
    ws = websocket.create_connection("ws://{0}:{1}/websocket".format(RT_SERVER_IP,RT_SERVER_PORT))

    ws.send("Hello World")  #not recognized
    ws.send("ECHO testing reply") #ECHO + space + command veifies the loop
    print("Sent")

    print("Received: {0}".format(ws.recv()))

    #Register interest to all room (-1) otherwise give room ID list to register to events
    regmsg = {"PUB_ID":-1, "MSG_TYPE":WEBSOCKET_WEB_MSG_TYPE, "PAYLOAD": {"MSG_CLASS":"REG_PUBLISHER"}}
    ws.send(json.dumps(regmsg))
    while True:
        result = ws.recv()
        print("Received: {0}".format(result))


    #do a clean close if program exist
    ws.close()
