__author__ = 'Olivier Van Cutsem'

from bmsinterface.bms_interface_config import DB_KEY_ENERGY_RELATED_SENSOR_ID

class BuildingEnergyRelatedEntityData(object):

    """
    The class BuildingEnergyRelatedEntityData is a generic class for storing data about any entity that is related to
    energy (Loads, Battery, Generations).
    """

    def __init__(self, related_sensors, name):
        """
        Constructor
        :param related_sensors: a dictionary of (sensor IDs, energy_type) pairs linked to this energy-related entity
        :param name: a dictionary of (sensor IDs, energy_type) pairs linked to this energy-related entity
        """
        self.__related_sensors = related_sensors
        self.__name = name
        self.__current_power = None

    def hasSensor(self, energy_type, sensor_id):
        """
        Return true if this energy-related entity has the sensor_id related to energy_type
        :param energy_type: a string representing the type of energy the sensor reads
        :param sensor_id: a numerical value representing the sensor ID
        :return: Boolean
        """
        if sensor_id in self.__related_sensors.keys():
            linked_energy_type = self.__related_sensors[sensor_id]
            if linked_energy_type == energy_type:
                return True

        return False

    #todo: basic manipulation

    @property
    def sensors(self):
        return self.__related_sensors

    @property
    def name(self):
        """
        Get the name of this energy related entity
        :return: a string representing the name of this energy related entity
        """
        return self.__name

    @name.setter
    def name(self, v):
        """
        Set the name of this energy related entity
        :v: a string representing the name of this energy related entity
        """
        self.__name = v

    @property
    def current_power(self):
        """
        Get the current produced power of this energy generation system
        :return: a numerical value representing the current produced power of this energy generation system
        """
        return self.__current_power

    @current_power.setter
    def current_power(self, v):
        """
        Set the current produced power of this energy generation system
        :v: a numerical value representing the current produced power of this energy generation system
        """
        self.__current_power = v
