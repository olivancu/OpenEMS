__author__ = 'Olivier Van Cutsem'

from bmsinterface.bms_restful_client import *
from building_data_management.room_data import BuildingRoomData
from ems_config import *
from bmsinterface.load_profile.ve_lp import *

from building_data_management.category_management.cat_tree import CategoryTree
from building_data_management.category_management.category_config import EMS_CATEGORY_STRUCTURE
from building_data_management.signal_and_constraint_management.constraint_data import TimeVaryingConstraint
from building_data_management.interface_management.generic_interfaces import *

import json


class BuildingDataManagementSystem:

    """
    This class structures the data of the building, mainly in two categories:
    - Rooms: the rooms information (volume, interfaces, etc) and comfort variables (temperatures, lum, etc)
    - Electrical entities: the information about the loads, battery and energy generators
    """

    def __init__(self):
        """
        Initialize BuildingDataManagementSystem object.
        """

        ### Get the rooms in the units and create the structure ###
        self.__rooms = dict()

        #Build the id -> name mapping table
        self.__room_name_mapping = dict()

        ### Store the loads, storage and generations data ###
        self.__energy_entities = CategoryTree(category_struct=EMS_CATEGORY_STRUCTURE)

    ##############################################
    ####### STRUCTURES INITIALIZATION ############
    ##############################################

    def initFromBMS(self, unit):
        """
        Make a set of API request in order to fill the main structures
        :remark: some PRINTs indicated the success of the initialization
        """

        roomdata = get_rooms_in_unit(unit)  # data such as name, surface, volume, etc

        rooms_interfaces = get_interface_geometry(unit)  # data about the interfaces separating rooms

        for r_id in roomdata.keys():
            self.__room_name_mapping[roomdata[r_id][DB_KEY_ROOM_NAME]] = r_id

        #Prepare a mapping from {Load,Gen,Stor} to {Sensor, Actuator}
        energy_entity_sensor_mapping = dict()
        for cat in ems_cat_main_list:
            energy_entity_sensor_mapping[cat] = dict()

        #Fill the data with sensors, actuators and interface info
        for r_id in roomdata.keys():
            this_room_name = roomdata[r_id][DB_KEY_ROOM_NAME]

            room_nature_type = None
            if DB_KEY_ROOM_NATURE in roomdata[r_id].keys() and DB_KEY_ROOM_TYPE in roomdata[r_id].keys():
                room_nature_type = (roomdata[r_id][DB_KEY_ROOM_NATURE], roomdata[r_id][DB_KEY_ROOM_TYPE])

            self.__rooms[r_id] = BuildingRoomData(roomdata[r_id][DB_KEY_ROOM_NAME],
                                                  (roomdata[r_id][DB_KEY_ROOM_SURFACE], roomdata[r_id][DB_KEY_ROOM_VOLUME]),
                                                  room_nature_type)

            ## Get the sensors of this room and store the corresponding information with comfort structure
            room_sensors_data = get_ambient_sensors_in_room(r_id)  # ambient sensors for comfort link
            for sensor_id in room_sensors_data.keys():
                type_measure = room_sensors_data[sensor_id][DB_KEY_COMFORT_TYPE]
                current_value = room_sensors_data[sensor_id][DB_KEY_COMFORT_VALUE]
                self.__rooms[r_id].initSensordata(type_measure, current_value, sensor_id)

            ## Get the list of thermal loads that influence the room temperature
            if self.__rooms[r_id].comfort[EMS_COMFORT_temperature] is not None:
                self.__rooms[r_id].comfort[EMS_COMFORT_temperature].loads = get_thermal_loads_in_room(r_id)

            ## Get the constraints linked to this room (temperature, etc)

            room_constraints = get_room_constraints(r_id)
            if room_constraints is not None:
                for constr_type in room_constraints.keys():
                    data = room_constraints[constr_type]

                    # Constraints on the comfort for this room
                    constr = data[DB_KEY_COMFORT_CONSTRAINTS]
                    constraint_object = TimeVaryingConstraint(constr[DB_KEY_COMFORT_TIMECONSTRAINTS_TIMEVECTOR],
                                                              constr[DB_KEY_COMFORT_TIMECONSTRAINTS_MINVECTOR],
                                                              constr[DB_KEY_COMFORT_TIMECONSTRAINTS_MAXVECTOR])
                    self.__rooms[r_id].set_comfort_constraint(constr_type, constraint_object)

                    # A reference value on the comfort for this room
                    if DB_KEY_COMFORT_REFERENCE_VALUE in data:
                        self.__rooms[r_id].set_reference_value(constr_type, data[DB_KEY_COMFORT_REFERENCE_VALUE])

            ## Get the actuator of this room and store the corresponding information
            room_actuator_data = get_actuators_in_room(r_id)
            self.__rooms[r_id].set_actuators(room_actuator_data)

            if r_id in rooms_interfaces.keys():  # If this room needs thermal-simu data
                for neigh_id in rooms_interfaces[r_id].keys():
                    interf_list = rooms_interfaces[r_id][neigh_id]
                    for i in interf_list:
                        interf_obj = self.initInterfaces(i)
                        self.__rooms[r_id].setInterface(neigh_id, interf_obj)  # Multiple interface with the outside

            ## For further energy-related entities of this room
            room_energy_sensor_data = get_energy_sensors_in_room(r_id)
            self.addSensorDependency(room_energy_sensor_data, energy_entity_sensor_mapping)


        # Initialize the structure containing the energy-related entities
        loads_data = get_loads_data(unit)  # Loads
        stor_data = get_storage_data(unit)  # Storage
        gen_data = get_generation_data(unit)  # Generation
        self.initEnergyEntities(loads_data, stor_data, gen_data, energy_entity_sensor_mapping)


    def initEnergyEntities(self, _loads, _storages, _generations, _sensor_mapping):
        """
        Initialize the class structure with bms data about loads, storage and generation.
        :param _loads: a dictionary (id, obj) where id is the BMS id of the load and obj is a dictionary containing load data
        :param _storages: a dictionary (id, obj) where id is the BMS id of the battery and obj is a dictionary containing battery data
        :param _generations: a dictionary (id, obj) where id is the BMS id of the generation and obj is a dictionary containing generation data
        :param _sensor_mapping: a dictionary that links entities id to sensor BMS ids
        :return: /
        """
        dict_entities = self.__energy_entities  # storing all the information about the energy-related entities

        # Loads
        for l_id in _loads.keys():
            load = _loads[l_id]
            type_load = load[DB_KEY_LOAD_CONTROL_TYPE]  # type of controllability
            lp_obj = load[DB_KEY_LOAD_LP]  # directly the LP object
            device_type = load[DB_KEY_LOAD_DEVICE_TYPE]  # device type
            add_model_param = load[DB_KEY_LOAD_MODEL_PARAM]  # model parameters

            # See if there are related sensors (normally there should be at least one..
            linked_sensors = dict()
            if l_id in _sensor_mapping[EMS_CATEGORY_ENTITY_LOAD]:
                linked_sensors = _sensor_mapping[EMS_CATEGORY_ENTITY_LOAD][l_id]

            # Get the appropriate class
            classname = EMS_CATEGORY_ENTITY_DATASTRUCTURE_CLASSNAMES[EMS_CATEGORY_ENTITY_LOAD][type_load]
            class_module_name = EMS_CATEGORY_ENTITY_DATASTRUCTURE_MODULES[EMS_CATEGORY_ENTITY_LOAD][type_load]
            load_class_module = __import__("building_data_management."+class_module_name, fromlist=[classname])
            load_class = getattr(load_class_module, classname)
            load_struct = load_class(name=load[DB_KEY_LOAD_NAME],
                                     lp=lp_obj,
                                     related_sensors=linked_sensors,
                                     additional_param=add_model_param)  # instantiate the object

            load_struct.current_power = load[DB_KEY_LOAD_CURRENT_VALUE]
            if device_type is not None:
                load_struct.device_type = device_type

            # Insert the pair (id, object) in the appropriate load category
            path = [EMS_CATEGORY_ENTITY_LOAD, type_load]
            dict_entities.insert(path, l_id, load_struct)

        for s_id in _storages.keys():
            storage = _storages[s_id]

            # See if there are related sensors (normally there should be at least one..
            linked_sensors = dict()
            if s_id in _sensor_mapping[EMS_CATEGORY_ENTITY_STORAGE]:
                linked_sensors = _sensor_mapping[EMS_CATEGORY_ENTITY_STORAGE][s_id]

            type_stor = storage[DB_KEY_STORAGE_TYPE]

            # Get the appropriate class
            classname = EMS_CATEGORY_ENTITY_DATASTRUCTURE_CLASSNAMES[EMS_CATEGORY_ENTITY_STORAGE][type_stor]
            class_module_name = EMS_CATEGORY_ENTITY_DATASTRUCTURE_MODULES[EMS_CATEGORY_ENTITY_STORAGE][type_stor]
            storage_class_module = __import__("building_data_management."+class_module_name, fromlist=[classname])
            storage_class = getattr(storage_class_module, classname)
            stor_struct = storage_class(name=storage[DB_KEY_STORAGE_NAME],
                                        capa=storage[DB_KEY_STORAGE_CAPA],
                                        related_sensors=linked_sensors)  # instantiate the object
            stor_struct.life_cyle = storage[DB_KEY_STORAGE_LIFE]
            stor_struct.efficiency = storage[DB_KEY_STORAGE_EFFICIENCY]
            stor_struct.current_soc = storage[DB_KEY_STORAGE_CURRENT_SOC]
            stor_struct.max_soc = storage[DB_KEY_STORAGE_MAX_SOC]
            stor_struct.min_soc = storage[DB_KEY_STORAGE_MIN_SOC]
            stor_struct.current_power = storage[DB_KEY_STORAGE_CURRENT_POWER]
            stor_struct.current_cycle = storage[DB_KEY_STORAGE_CURRENT_CYCLE]

            stor_struct.model_parameters = json.loads(storage[DB_KEY_STORAGE_MODEL])

            # Add more info for the PHEV
            stor_struct.set_schedule_constraints(storage[DB_KEY_PHEV_SCHEDULE_ARRIVE_TIME],
                                                 storage[DB_KEY_PHEV_SCHEDULE_LEAVE_TIME])
            stor_struct.discharge_allowed = storage[DB_KEY_PHEV_ALLOW_DISCHARGE]

            # Insert the pair (id, object) in the appropriate load category
            path = [EMS_CATEGORY_ENTITY_STORAGE, type_stor]
            dict_entities.insert(path, s_id, stor_struct)

        # GenerationDB_KEY_GENERATION_NAME: data['name'],

        for g_id in _generations.keys():
            generation = _generations[g_id]

            # See if there are related sensors (normally there should be at least one..
            linked_sensors = dict()
            if g_id in _sensor_mapping[EMS_CATEGORY_ENTITY_GENERATION]:
                linked_sensors = _sensor_mapping[EMS_CATEGORY_ENTITY_GENERATION][g_id]

            # Get the appropriate class
            classname = EMS_CATEGORY_ENTITY_DATASTRUCTURE_CLASSNAMES[EMS_CATEGORY_ENTITY_GENERATION]
            class_module_name = EMS_CATEGORY_ENTITY_DATASTRUCTURE_MODULES[EMS_CATEGORY_ENTITY_GENERATION]
            storage_class_module = __import__("building_data_management."+class_module_name, fromlist=[classname])
            generation_class = getattr(storage_class_module, classname)
            gen_struct = generation_class(name=generation[DB_KEY_GENERATION_NAME],
                                          nom_power=generation[DB_KEY_GENERATION_NOM_POWER],
                                          related_sensors=linked_sensors)  # instantiate the object
            gen_struct.generation_type = generation[DB_KEY_GENERATION_TYPE]
            gen_struct.current_power = generation[DB_KEY_GENERATION_CURRENT_POWER]

            gen_struct.model_parameters = json.loads(generation[DB_KEY_GENERATION_MODEL_PARAM])

            # Insert the pair (id, object) in the appropriate load category
            path = [EMS_CATEGORY_ENTITY_GENERATION]
            dict_entities.insert(path, g_id, gen_struct)

    def initInterfaces(self, db_data):

        """
        Initialize rooms interface from the bms data
        :param db_data: a formatted dictionary containing interfaces data
        :return:
        """



        obj_ret = None

        if db_data == {}:
            return obj_ret

        # TODO: Luminosity model and IRR-TO-HEAT coefficient
        type_of_interface = db_data[DB_KEY_INTERFACE_TYPE_KEY]

        DB_KEY_INTERFACE_WALL_KEY = 'interface_wall'
        DB_KEY_INTERFACE_WINDOW_KEY = 'interface_window'
        DB_KEY_INTERFACE_DOOR_KEY = 'interface_door'
        if type_of_interface == DB_KEY_INTERFACE_WALL_KEY:
            thermal_param = db_data[DB_KEY_INTERFACE_THERMAL_PARAM]
            obj_ret = WallInterface(db_data[DB_KEY_INTERFACE_SURFACE],
                                    db_data[DB_KEY_INTERFACE_THICKNESS],
                                    thermal_param)

        elif type_of_interface == DB_KEY_INTERFACE_WINDOW_KEY:
            thermal_param = db_data[DB_KEY_INTERFACE_THERMAL_PARAM]
            luminosity_param = None
            irradiance_to_heat_param = None
            obj_ret = WindowInterface(db_data[DB_KEY_INTERFACE_SURFACE],
                                     db_data[DB_KEY_INTERFACE_THICKNESS],
                                     thermal_param, luminosity_param, irradiance_to_heat_param)
            obj_ret.openingActuator = db_data[DB_KEY_INTERFACE_OPENING_ACTUATOR]

        elif type_of_interface == DB_KEY_INTERFACE_DOOR_KEY:
            thermal_param = db_data[DB_KEY_INTERFACE_THERMAL_PARAM]
            obj_ret = DoorInterface(db_data[DB_KEY_INTERFACE_SURFACE],
                                 db_data[DB_KEY_INTERFACE_THICKNESS],
                                 thermal_param)
            obj_ret.openingActuator = db_data[DB_KEY_INTERFACE_OPENING_ACTUATOR]

        return obj_ret

    def addSensorDependency(self, energy_sensor_data, mapping_table):
        """
        Link sensors data (identified by their sensor id) to a specific "energy type" entity
        :param energy_sensor_data: a dict (id, obj) where obj is a formatted dictionary  containing data about sensor id
        :param mapping_table: the dictionary mapping energy entities to their sensors ids
        :return:
        """
        for sens_id in energy_sensor_data.keys():
            data_sensor = energy_sensor_data[sens_id]

            cat_entity = data_sensor.pop(DB_KEY_ENERGY_ENTITY_RELATED_TYPE)  # remove this key from the dict
            entity_id = data_sensor.pop(DB_KEY_ENERGY_ENTITY_RELATED_ID)  # remove this key from the dict

            if entity_id not in mapping_table[cat_entity]:
                mapping_table[cat_entity][entity_id] = dict()

            mapping_table[cat_entity][entity_id][sens_id] = data_sensor[DB_KEY_ENERGY_MEASURE_TYPE]  # id sensor -> type

    def addActuatorDependency(self, energy_actuator_data, mapping_table):
        """
        Actuator ID -> Energy entity ID
        :param energy_actuator_data: data about the actuator
        :param mapping_table: the mapping "Actuator -> energy entity" table
        :return: /
        """
        for act_id in energy_actuator_data.keys():
            data_actuator = energy_actuator_data[act_id]
            mapping_table[act_id] = data_actuator

    def updateComfortRoom(self, sensor_id, type_comfort, value):
        """
        Search the room that contains the sensor_id for a given type_comfort and update the value
        :param sensor_id: the id of the sensor linked to a room
        :param type_comfort: the type of comfort the sensor senses
        :param value: the new value
        :return: /
        """

        for r_obj in self.__rooms.values():
            if r_obj.hasSensor(sensor_id, type_comfort):
                r_obj.set_comfort_value(type_comf=type_comfort, value=value)
                #print("updating room {0} for comfort type '{1}' with value {2}".format(r_obj.name, type_comfort, value))
                break

    def updateEnergyRelatedEntity(self, sensor_id, type_energy, value):
        """
        Search the entity linked to the sensor_id for a given type_energy and update the value
        :param sensor_id: the id of the sensor linked to an electrical entity
        :param type_energy: the type of entity (from
        :param value:
        :return:
        """
        set_entities = self.__energy_entities.getEntitiesValuesList()  # get all the objects related to energy

        for entity in set_entities:
            if entity.hasSensor(type_energy, sensor_id):
                # Check the type of variable
                if type_energy == EMS_ENERGY_power:
                    entity.current_power = value
                elif type_energy == EMS_ENERGY_soc:
                    entity.current_soc = value
                elif type_energy == EMS_ENERGY_nb_cycle:
                    entity.current_cycle = value
                #print("updating entity {0} for quantity type '{1}' with value {2}".format(entity.name, type_energy, value))


                                        ############################################
                                        ####### STRUCTURES MANIPULATION ############
                                        ############################################

    def get_entity_list(self, type_entity):
        """
        Get the list of energy-related entities of type "type_entity".
        :param type_entity:
        :return: [(id, obj), ...] list for the type_entity where id is the unique ID of the entity of type type_entity
        and obj is a sub-type of "BuildingEnergyRelatedEntityData"
        """
        list_entities = list()

        # get all the IDs
        ids = self.__energy_entities.getEntitiesIdList(type_entity)

        # then get the object
        for i in ids:
            (obj, path) = self.__energy_entities.get(i, type_entity)
            list_entities.append((i, obj))

        return list_entities

    def get_energy_entity_obj(self, e_type, e_id):
        """
        Get the details about any energy-related entity
        :param e_type: the type of energy-related entity (in EMS_CATEGORY_)
        :param e_id: the id of the energy-related entity
        :return: a BuildingEnergyRelatedEntityData object describing the corresponding energy-related entity
        """

        [o, p] = self.__energy_entities.get(e_id, e_type)
        return o

    @property
    def room_list(self):
        """
        List of rooms in the building
        :return: a list of BuildingRoomData objects representing the rooms of the building
        """
        return self.__rooms.values()

    @property
    def room_ids_list(self):
        """
        List of rooms'IDs in the building
        :return: a list of IDs that uniquely identify rooms present in the building
        """
        return self.__rooms.keys()

    def get_rooms_ids_list(self, nature=None):
        """
        List of rooms'IDs in the building of a specific nature
        :return: a list of IDs that uniquely identify rooms present in the building
        """
        if nature is None:
            return self.room_ids_list

        ret = []
        for r_id in self.__rooms.keys():
            if self.room(r_id).nature == nature:
                ret.append(r_id)

        return ret

    def room(self, r_id):
        """
        The room structure determined by its ID "r_id"
        :return: a BuildingRoomData object that describe the room of ID "r_id"
        """
        return self.__rooms[r_id]
