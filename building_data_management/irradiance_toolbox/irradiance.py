__author__ = 'Max Chevron'

import numpy as np
from enum import Enum

   #####################################################
################### BUILDING THERMAL SIMULATION ###########
   #####################################################

BLIND_POSITION_OPEN = 2
BLIND_POSITION_DIRECT_BLOCK = 2
BLIND_POSITION_CLOSED = 0

def building_thermal_effect(time_info, irr_data, coord_info, WINDOW_KL, WINDOW_NG, GLASS_AZIMUTH, WALL_TILT,  GLAZING, BLINDS_POSITION, DIRECT_BLOCK_TRANSM):
    """
    This function returns the power (W/m2) transferred from the incoming irradiance to the building

    :param time_info: a tuple (time_d, time_h, time_m)
    :param irr_data: a tuple (irr_glob, irr_diff) containing the irradiance components
    :param coord_info: a tuple (LAT, LONG)
    :return: a float, representing a power (W/m2) value
    """

    (time_d, time_h, time_m) = time_info
    (LATITUDE, LONGITUDE) = coord_info
    irr_glob, irr_diff = irr_data  # The two components of the irradiance
    irr_glob = max(0, irr_glob)
    irr_diff = max(0, irr_diff)
    irr_direct = irr_glob-irr_diff  # radiation that comes as a beam from the sun position

    times = time_h*100+time_m  # 3.20pm turns our to be 1520
    equation_of_time = equation_of_time_spencer71(time_d)  # in minutes
    hour_angle = hour_angle_fn(times, LONGITUDE, equation_of_time)  # radians
    declination = declination_cooper69(time_d)  # radians
    solar_zenith = solar_zenith_analytical(np.radians(LATITUDE), hour_angle, declination)  # radians
    solar_azimuth = solar_azimuth_analytical(np.radians(LATITUDE), hour_angle, declination, solar_zenith)  # radians
    solar_incidence_angle = aoi(np.radians(WALL_TILT), np.radians(GLASS_AZIMUTH), solar_zenith, solar_azimuth)  # radians

    total_transmission = 0
    if BLINDS_POSITION == BLIND_POSITION_OPEN:
        transmittance, transmittance_60, reflectance, absorptance = beam_glazing_fn(solar_incidence_angle, WINDOW_KL, WINDOW_NG, GLAZING) # glass properties
        diffuse_radiation = diffuse_rad_fn(irr_diff, np.radians(WALL_TILT))  # diffuse radiation perceived by a tilted surface
        beam_radiation = irradiation_fn(irr_direct, solar_incidence_angle)  # direct radiation perceived by a tilted surface
        beam_transmission, diffuse_transmission = beam_and_diffuse_transmission(beam_radiation, diffuse_radiation,  transmittance, transmittance_60)
        total_transmission = (beam_transmission + diffuse_transmission)
    if BLINDS_POSITION == BLIND_POSITION_DIRECT_BLOCK:
        total_transmission = irr_glob*DIRECT_BLOCK_TRANSM
    if BLINDS_POSITION == BLIND_POSITION_CLOSED:
        total_transmission = 0

    return total_transmission  # in Watts [W]


   #####################################################
################### PV EQUIVALENT IRRADIANCE ##############
   #####################################################


def tilted_surface_irradiance(time_info, irr_data, tilt, coord_info, SURFACE_AZIMUTH):
    """
    This function returns the irradiance perceived by a tilted surface, given global and diffuse incoming irradiance
    :param time_info: a tuple (time_d, time_h, time_m)
    :param irr_data: a tuple (irr_glob, irr_diff) containing the irradiance components
    :return: a float, representing an irradiance (W/m2) value
    """

    (time_d, time_h, time_m) = time_info
    (LATITUDE, LONGITUDE) = coord_info

    irr_glob, irr_diff = irr_data  # The two component of the irradiance
    irr_glob = max(0, irr_glob)
    irr_diff = max(0, irr_diff)
    irr_direct = irr_glob-irr_diff  # radiation that comes as a beam from the sun position

    times = time_h*100+time_m  # 3.20pm turns our to be 1520
    equation_of_time = equation_of_time_spencer71(time_d)  # in minutes
    hour_angle = hour_angle_fn(times, np.radians(LONGITUDE), equation_of_time)  # radians
    declination = declination_cooper69(time_d)  # radians
    solar_zenith = solar_zenith_analytical(np.radians(LATITUDE), hour_angle, declination)  # radians
    solar_azimuth = solar_azimuth_analytical(np.radians(LATITUDE), hour_angle, declination, solar_zenith)  # radians
    solar_incidence_angle = aoi(np.radians(tilt), np.radians(SURFACE_AZIMUTH), solar_zenith, solar_azimuth)  # radians
    diffuse_radiation = diffuse_rad_fn(irr_diff, np.radians(tilt))  # diffuse radiation perceived by a tilted surface
    beam_radiation = irradiation_fn(irr_direct, solar_incidence_angle)  # direct radiation perceived by a tilted surface
    PV_irradiation = beam_radiation + diffuse_radiation  # diffuse radiation considered to be perceived 100% no matter the tilt

    return PV_irradiation  # in Watts per square meter [W/m2]


def equation_of_time_spencer71(dayofyear):
    """
    Equation of time from Duffie & Beckman and attributed to Spencer (1971) and
    Iqbal (1983).

    The coefficients correspond to the online copy of the `Fourier paper`_ [1]_
    in the Sundial Mailing list that was posted in 1998 by Mac Oglesby from his
    correspondence with Macquarie University Prof. John Pickard who added the
    following note.

        In the early 1970s, I contacted Dr Spencer about this method because I
        was trying to use a hand calculator for calculating solar positions,
        etc. He was extremely helpful and gave me a reprint of this paper. He
        also pointed out an error in the original: in the series for E, the
        constant was printed as 0.000075 rather than 0.0000075. I have corrected
        the error in this version.

    There appears to be another error in formula as printed in both Duffie &
    Beckman's [2]_ and Frank Vignola's [3]_ books in which the coefficient
    0.04089 is printed instead of 0.040849, corresponding to the value used in
    the Bird Clear Sky model implemented by Daryl Myers [4]_ and printed in both
    the Fourier paper from the Sundial Mailing List and R. Hulstrom's [5]_ book.

    .. _Fourier paper: http://www.mail-archive.com/sundial@uni-koeln.de/msg01050.html

    Parameters
    ----------
    dayofyear : numeric

    Returns
    -------
    equation_of_time : numeric
        Difference in time between solar time and mean solar time in minutes.

    References
    ----------
    .. [1] J. W. Spencer, "Fourier series representation of the position of the
       sun" in Search 2 (5), p. 172 (1971)

    .. [2] J. A. Duffie and W. A. Beckman,  "Solar Engineering of Thermal
       Processes, 3rd Edition" pp. 9-11, J. Wiley and Sons, New York (2006)

    .. [3] Frank Vignola et al., "Solar And Infrared Radiation Measurements",
       p. 13, CRC Press (2012)

    .. [5] Daryl R. Myers, "Solar Radiation: Practical Modeling for Renewable
       Energy Applications", p. 5 CRC Press (2013)

    .. [4] Roland Hulstrom, "Solar Resources" p. 66, MIT Press (1989)

    See Also
    --------
    equation_of_time_pvcdrom
    """
    day_angle = (2. * np.pi / 365.) * (dayofyear - 1) # Calculates the day angle for the Earth's orbit around the Sun.
    # convert from radians to minutes per day = 24[h/day] * 60[min/h] / 2 / pi
    return (1440.0 / 2 / np.pi) * (0.0000075 +
        0.001868 * np.cos(day_angle) - 0.032077 * np.sin(day_angle) -
        0.014615 * np.cos(2.0 * day_angle) - 0.040849 * np.sin(2.0 * day_angle) # in minutes
    )


def hour_angle_fn(times, LONGITUDE, equation_of_time):
    """
    Hour angle in local solar time. Zero at local solar noon.

    Parameters
    ----------
    times : :class:`pandas.DatetimeIndex`
        Corresponding timestamps, must be timezone aware.
    longitude : numeric
        Longitude in degrees
    equation_of_time : numeric
        Equation of time in minutes.

    Returns
    -------
    hour_angle : numeric
        Hour angle in local solar time in radians.

    References
    ----------
    [1] J. A. Duffie and W. A. Beckman,  "Solar Engineering of Thermal
    Processes, 3rd Edition" pp. 13, J. Wiley and Sons, New York (2006)

    [2] J. H. Seinfeld and S. N. Pandis, "Atmospheric Chemistry and Physics"
    p. 132, J. Wiley (1998)

    [3] Daryl R. Myers, "Solar Radiation: Practical Modeling for Renewable
    Energy Applications", p. 5 CRC Press (2013)

    See Also
    --------
    equation_of_time_Spencer71
    equation_of_time_pvcdrom
    """

    loc_stan_mer_time = 15*round(LONGITUDE/15)
    loc_sol_time = times + 4*(LONGITUDE - loc_stan_mer_time) + equation_of_time
    hour_angle = 15.*(loc_sol_time - 1200)/100 # hour angle in degrees
    return np.radians(hour_angle)


def declination_cooper69(dayofyear):
    """
    Solar declination from Duffie & Beckman [1] and attributed to Cooper (1969)

    .. warning::
        Return units are radians, not degrees.

    Declination can be expressed using either sine or cosine:

    .. math::

       \\delta = 23.45 \\sin \\left( \\frac{2 \\pi}{365} \\left(n_{day} + 284
       \\right) \\right) = -23.45 \\cos \\left( \\frac{2 \\pi}{365}
       \\left(n_{day} + 10 \\right) \\right)

    Parameters
    ----------
    dayofyear : numeric

    Returns
    -------
    declination (radians) : numeric
        Angular position of the sun at solar noon relative to the plane of the
        equator, approximately between +/-23.45 (degrees).

    References
    ----------
    [1] J. A. Duffie and W. A. Beckman,  "Solar Engineering of Thermal
    Processes, 3rd Edition" pp. 13-14, J. Wiley and Sons, New York (2006)

    [2] J. H. Seinfeld and S. N. Pandis, "Atmospheric Chemistry and Physics"
    p. 129, J. Wiley (1998)

    [3] Daryl R. Myers, "Solar Radiation: Practical Modeling for Renewable
    Energy Applications", p. 4 CRC Press (2013)

    See Also
    --------
    declination_spencer71
    """
    day_angle = (2. * np.pi / 365.) * (dayofyear - 1)
    return np.deg2rad(23.45 * np.sin(day_angle + (2.0 * np.pi / 365.0) * 285.0)) # in rad


def solar_zenith_analytical(latitude, hour_angle, declination):
    """
    Analytical expression of solar zenith angle based on spherical trigonometry.

    .. warning::
        The analytic form neglects the effect of atmospheric refraction.

    Parameters
    ----------
    latitude : numeric
        Latitude of location in radians.
    hour_angle : numeric
        Hour angle in the local solar time in radians.
    declination : numeric
        Declination of the sun in radians.

    Returns
    -------
    zenith : numeric
        Solar zenith angle in radians.

    References
    ----------
    [1] J. A. Duffie and W. A. Beckman,  "Solar Engineering of Thermal
    Processes, 3rd Edition" pp. 14, J. Wiley and Sons, New York (2006)

    [2] J. H. Seinfeld and S. N. Pandis, "Atmospheric Chemistry and Physics"
    p. 132, J. Wiley (1998)

    [3] Daryl R. Myers, "Solar Radiation: Practical Modeling for Renewable
    Energy Applications", p. 5 CRC Press (2013)

    `Wikipedia: Solar Zenith Angle <https://en.wikipedia.org/wiki/Solar_zenith_angle>`_

    `PVCDROM: Sun's Position <http://www.pveducation.org/pvcdrom/2-properties-sunlight/suns-position>`_

    See Also
    --------
    declination_spencer71
    declination_cooper69
    hour_angle
    """
    return np.arccos(
        np.cos(declination) * np.cos(latitude) * np.cos(hour_angle) +
        np.sin(declination) * np.sin(latitude) # in rad
    )


def solar_azimuth_analytical(latitude, hour_angle, declination, solar_zenith):
    """
    Analytical expression of solar azimuth angle based on spherical
    trigonometry.

    Parameters
    ----------
    latitude : numeric
        Latitude of location in radians.
    hour_angle : numeric
        Hour angle in the local solar time in radians.
    declination : numeric
        Declination of the sun in radians.
    zenith : numeric
        Solar zenith angle in radians.

    Returns
    -------
    azimuth : numeric
        Solar azimuth angle in radians.

    References
    ----------
    [1] J. A. Duffie and W. A. Beckman,  "Solar Engineering of Thermal
    Processes, 3rd Edition" pp. 14, J. Wiley and Sons, New York (2006)

    [2] J. H. Seinfeld and S. N. Pandis, "Atmospheric Chemistry and Physics"
    p. 132, J. Wiley (1998)

    [3] `Wikipedia: Solar Azimuth Angle
    <https://en.wikipedia.org/wiki/Solar_azimuth_angle>`_

    [4] `PVCDROM: Azimuth Angle <http://www.pveducation.org/pvcdrom/2-properties
    -sunlight/azimuth-angle>`_

    See Also
    --------
    declination_spencer71
    declination_cooper69
    hour_angle
    solar_zenith_analytical
    """

    s_a_a = np.sign(hour_angle) * np.abs(np.arccos((np.cos(solar_zenith) * np.sin(
    latitude) - np.sin(declination)) / (np.sin(solar_zenith) * np.cos(
    latitude)))) + np.pi # in rad

    return s_a_a # in rad


def aoi(tilt, surface_azimuth, solar_zenith, solar_azimuth):
    """
    Calculates the angle of incidence of the solar vector on a surface.
    This is the angle between the solar vector and the surface normal.

    Input all angles in radians.

    Parameters
    ----------
    surface_tilt : numeric
        Panel tilt from horizontal.
    surface_azimuth : numeric
        Panel azimuth from north.
    solar_zenith : numeric
        Solar zenith angle.
    solar_azimuth : numeric
        Solar azimuth angle.

    Returns
    -------
    aoi : numeric
        Angle of incidence in radians.
    """

    # Calculates the dot product of the solar vector and the surface normal (all inputs in degrees).
    projection = ( np.cos(tilt) * np.cos(solar_zenith) +
                np.sin(tilt) * np.sin(solar_zenith) *
                np.cos(solar_azimuth - surface_azimuth))

    aoi_value = np.arccos(projection)

    return aoi_value # in radians


def diffuse_rad_fn(irr_diff, tilt):
    """
    diffuse radiation is based on a plane surface.
    This function keeps only the diffuse radiation from ground to 90 degrees vs normal of the surface
    inputs:
    - irr_diff: diffuse irradiation perceived on a horizontal surface (weather data)
    - tilt: tilt angle of the surface in radians
    output:
    - diffuse_radiation: diffuse radiation perceived by a tilted surface
    """

    diffuse_radiation = irr_diff*(1 + np.cos(tilt))/2

    return diffuse_radiation


def irradiation_fn(incident_radiation, solar_incidence_angle):
# Trigonometry function to return radiation perceived by a solar PV module or window
    """
    Trigonometry function to return radiation perceived by a solar PV module or window
    inputs:
    - incident_radiation: radiation that comes as a beam from the sun position (weather data)
    - solar_incidence_angle: This is the angle between the solar vector and the surface normal.
    output:
    - radiation: direct radiation perceived by a tilted surface
    """

    radiation = max(0, incident_radiation * np.cos(solar_incidence_angle))

    return radiation # (in W/m2)


def beam_glazing_fn(solar_incidence_angle, WINDOW_KL, WINDOW_NG, GLAZING):
    """
    returns that optical properties of the glass
    inputs:
    - solar_incidence_angle: This is the angle between the solar vector and the surface normal.
    - WINDOW_KL: exctinction coeff. times glazing thickness (most common is glazing thickness of 20mmm
    - WINDOW_NG: refractive index (main glass are between 1.52 and 1.54 ...)
    - GLAZING: 1 for single glazing and 2 for double // impact only on the transmittance and impact on output is almost negligeable
    outputs:
    - transmittance: 0 to 1 coefficient indicating how much radiation goes through the glass
    - transmittance_60: additional computation for diffuse radiation transmission
    - reflectance: 0 to 1 coefficient to indicate how much radiation is reflected by the glass
    - absorptance: 0 to 1 coefficient to indicate how much radiation is absorbed by the glass
    """

    angle_of_refraction = np.arcsin(np.sin(solar_incidence_angle)/WINDOW_NG)
    reflectivity = 0.5*((np.sin(solar_incidence_angle - angle_of_refraction)/np.sin(solar_incidence_angle + angle_of_refraction))**2
                        + (np.tan(solar_incidence_angle - angle_of_refraction)/np.tan(solar_incidence_angle + angle_of_refraction))**2)
    alpha = np.exp(- WINDOW_KL/np.sqrt(1-(np.sin(solar_incidence_angle)/WINDOW_NG)**2))
    transmittance = ((1-reflectivity)**2*alpha)/(1-reflectivity**2*alpha**2)
    if transmittance<0:
        transmittance=0
    reflectance = reflectivity + ((reflectivity*(1-reflectivity)**2*alpha**2)/(1-reflectivity**2*alpha**2))
    absorptance = 1 - reflectance - transmittance

    if GLAZING == 2:
        transmittance = transmittance**2/(1-reflectance**2)

    # ------ transmitance_60 calculation --------- used for diffuse transmitted solar radiation accordingly to "7.3 Solar Radiation Transmitted by Windows.pdf"
    # same calculations as above yet with a solar incidence angle of 60 degrees
    sia_60 = np.radians(60)
    angle_of_refraction = np.arcsin(np.sin(sia_60)/WINDOW_NG)
    reflectivity = 0.5*((np.sin(sia_60 - angle_of_refraction)/np.sin(sia_60 + angle_of_refraction))**2
                        + (np.tan(sia_60 - angle_of_refraction)/np.tan(sia_60 + angle_of_refraction))**2)
    alpha = np.exp(- WINDOW_KL/np.sqrt(1-(np.sin(sia_60)/WINDOW_NG)**2))
    transmittance_60 = max(0,((1-reflectivity)**2*alpha)/(1-reflectivity**2*alpha**2))
    # -------------

    return transmittance, transmittance_60, reflectance, absorptance


def beam_and_diffuse_transmission(beam_radiation, diffuse_radiation, transmittance, transmittance_60):
    """
    returns the direct and diffuse radiation that goes through the glass
    inputs:
    - beam_radiation: radiation that comes as a beam from the sun position (weather data)
    - diffuse_radiation: radiation scattered by the atmosphere (weather data)
    - transmittance: 0 to 1 coefficient indicating how much radiation goes through the glass
    - transmittance_60: additional computation for diffuse radiation transmission
    outputs:
    - beam_transmission: beam (direct) radiation that goes through the glass [W/m2]
    - diffuse_transmission: diffuse radiation that goes through the glass [W/m2]
    """

    beam_transmission = beam_radiation*transmittance
    diffuse_transmission = diffuse_radiation*transmittance_60
    return beam_transmission, diffuse_transmission # both in [W/m2]
