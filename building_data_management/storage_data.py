__author__ = 'Olivier Van Cutsem'

from building_data_management.energyrelated_data import BuildingEnergyRelatedEntityData
import numpy

EMS_BAT_GEN_PARAM_CAPA = 'nominal_capacity'
EMS_BAT_GEN_PARAM_SOC_INIT  = 'initial_soc'
EMS_BAT_GEN_PARAM_CH_EFF = 'charging_efficiency'
EMS_BAT_GEN_PARAM_DISCH_EFF = 'discharging_efficiency'
EMS_BAT_GEN_PARAM_CYCLES = 'life_cycles'
EMS_BAT_GEN_PARAM_LEAK = 'leakage_coeff'
EMS_BAT_GEN_PARAM_MAX_P_CH = 'max_power_charge'
EMS_BAT_GEN_PARAM_MAX_P_DISCH = 'max_power_discharge'

EMS_STORAGE_SLEEP_PARAM = "sleeping_param"
EMS_STORAGE_SLEEP_SLIDING_WINDOW = "sliding_window_size"
EMS_STORAGE_SLEEP_SLIDING_WINDOW_WEIGHT = "sliding_window_weight"


class BuildingStorageData(BuildingEnergyRelatedEntityData):
    """
    A class that stores the data structure of any Energy Storage System
    """

    BATTERY_EPS_SOC = 0.01  # less than 1% is considered as 0

    def __init__(self, name, capa, related_sensors):
        """
        Constructor
        :param name: a string representing the name of the Energy Storage System
        :param capa: a numerical value representing the capacity of the Energy Storage System
        :param related_sensors: a dictionary listing the sensors linked to this Energy Storage System
        """
        super(BuildingStorageData, self).__init__(related_sensors=related_sensors, name=name)

        self.__capacity = capa

        # Generic data
        self.__life_cyle = None
        self.__efficiency = None
        self.__min_soc = 0
        self.__max_soc = 1

        # Dynamic values
        self.__current_soc = None
        self.__current_cycle = None

        # Model
        self.__model_parameters = None

    @property
    def capacity(self):
        """
        Get the capacity of this Energy Storage System
        :return: a numerical value representing the capacity of this Energy Storage System
        """
        return self.__capacity

    @capacity.setter
    def capacity(self, c):
        """
        set the capacity of this Energy Storage System
        :c: a numerical value representing the capacity of this Energy Storage System
        """
        self.__capacity = c

    @property
    def current_soc(self):
        """
        Get the current State of Charge of this Energy Storage System
        :return: a numerical value representing the current State of Charge of this Energy Storage System
        """
        return self.__current_soc

    @current_soc.setter
    def current_soc(self, v):
        """
        Set the current State of Charge of this Energy Storage System
        :v: a numerical value representing the current State of Charge of this Energy Storage System
        """
        self.__current_soc = v

    @property
    def life_cyle(self):
        """
        Get the total life cycles of this Energy Storage System
        :return: an integer value representing the total life cycles of this Energy Storage System
        """
        return self.__life_cyle

    @life_cyle.setter
    def life_cyle(self, l):
        """
        Set the remaining life cycles of this Energy Storage System
        :l: an integer value representing the remaining life cycles of this Energy Storage System
        """
        self.__life_cyle = l

    @property
    def leak_coeff(self):
        """
        Get the leakage coefficient of this Energy Storage System
        :return: a numerical value representing the leakage coefficient of this Energy Storage System
        """
        return self.__model_parameters[EMS_BAT_GEN_PARAM_LEAK]

    @property
    def efficiency(self):
        """
        Get the round-trip efficiency of this Energy Storage System
        :return: a numerical value representing the round-trip efficiency of this Energy Storage System
        """
        return self.__efficiency

    @efficiency.setter
    def efficiency(self, e):
        """
        Set the round-trip efficiency of this Energy Storage System
        :e: a numerical value representing the round-trip efficiency of this Energy Storage System
        """
        self.__efficiency = e

    @property
    def current_cycle(self):
        """
        Get the remaining life cycles of this Energy Storage System
        :return: an integer value representing the remaining life cycles of this Energy Storage System
        """
        return self.__current_cycle

    @current_cycle.setter
    def current_cycle(self, c):
        """
        Set the remaining life cycles of this Energy Storage System
        :c: an integer value representing the remaining life cycles of this Energy Storage System
        """
        self.__current_cycle = c

    @property
    def model_parameters(self):
        """
        Get the model parameters of this Energy Storage System
        :return: a structured dictionary representing the model parameters of this Energy Storage System
        """
        return self.__model_parameters

    @model_parameters.setter
    def model_parameters(self, m):
        """
        Set the model parameters of this Energy Storage System
        :m: a structured dictionary representing the model parameters of this Energy Storage System
        """
        self.__model_parameters = m

    @property
    def is_empty(self):
        """
        Battery empty?
        :return: a boolean value, true if the battery is empty, false otherwise
        """
        return self.__current_soc <= self.BATTERY_EPS_SOC

    @property
    def is_full(self):
        """
        Battery full?
        :return: a boolean value, true if the battery is full, false otherwise
        """
        return self.__current_soc >= 1-self.BATTERY_EPS_SOC

    @property
    def min_soc(self):
        """
        The minimum allowable State of Charge
        :return: a float value in [0;1]
        """
        return self.__min_soc

    @min_soc.setter
    def min_soc(self, v):
        """
        The minimum allowable State of Charge
        :return: a float value in [0;1]
        """
        self.__min_soc = v

    @property
    def max_soc(self):
        """
        The maximin allowable State of Charge
        :return: a float value in [0;1]
        """
        return self.__max_soc

    @max_soc.setter
    def max_soc(self, v):
        """
        The maximin allowable State of Charge
        :return: a float value in [0;1]
        """
        self.__max_soc = v

    def max_power(self, mode, soc=None):
        """
        Return the maximum charging/discharging power, at a specific State of Charge if provided
        :param mode: positive value for charging, negative for discharging
        :param soc: a float between 0 and 1
        :return: a float
        """
        # Which data to work with ?
        if mode > 0:  # charge
            data = self.__model_parameters[EMS_BAT_GEN_PARAM_MAX_P_CH]
        else:
            data = self.__model_parameters[EMS_BAT_GEN_PARAM_MAX_P_DISCH]

        if type(data) is not dict:
            return data

        # P as a function of SoC
        soc_list = data['soc']
        p_list = data['p']

        if soc is None:  # Mean value in possible SoC
            return soc_list, p_list
        else:  # The closest value to the specified SoC
            soc = min(soc_list, key=lambda x: abs(x - soc))
            idx = soc_list.index(soc)
            return p_list[idx]

    def max_power_charge(self, soc=None):
        """
        Get the maximum charging power of this Energy Storage System
        :param soc: a float value representing the state of charge
        :return:
        IF the max charging power is a constant, return the constant (float (>= 0)), ELSE:

        IF soc is specified:
           a float (>= 0) representing the maximum charging power of this Energy Storage System at SoC
        ELSE:
           a tuple (soc_list, p_list) where p_list contains the values of P_MAX corresponding to the values in soc_list
        """

        return self.max_power(1, soc)

    def min_power_discharge(self, soc=None):
        """
        Get the minimum charging power of this Energy Storage System
        :param soc: a float value representing the state of charge
        :return:
        IF the min charging power is a constant, return the constant (float (>= 0)), ELSE:

        IF soc is specified:
           a float (>= 0) representing the minimum charging power of this Energy Storage System at SoC
        ELSE:
           a tuple (soc_list, p_list) where p_list contains the values of P_MIN corresponding to the values in soc_list
        """

        return self.max_power(-1, soc)
