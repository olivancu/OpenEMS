__author__ = 'Olivier Van Cutsem'

from bmsinterface.load_profile.activity import *
from signal_and_constraint_management.signal_data import PiecewiseConstantSignal

ACTUATOR_TYPE_ACTION_DIMMER = 'DIMMER'  # dimmer: typically an opener for a valve, blind, window
ACTUATOR_TYPE_ACTION_BINARY = 'RELAY'  # ON/OFF actuator

# Opener specific
VE_VOPENER_VALUE_WHEN_FULLY_OPEN = "output_val_fully_opened"

class BuildingActuatorData(object):
    """
    A class that stores the data structure of any actuator
    """

    def __init__(self, name, lp, additional_param=None):
        """
        Constructor
        :param name: a string representing the name of the load
        :param lp: a LoadProfile object representing the load profile of the load
        :param additional_param: a dictionary listing additional parameters
        """

        self.__name = name

        self.__load_profile_act = lp  # This Profile Activities structure contains all the characteristics about the user actions on the actuator

        self.__current_value = None  # The current status of the actuator

        self.__additional_param = additional_param  # Additional parameter for modelling the actuator behavior

        self.__action_type = None  # The type of action on this actuator

        self.__rel_energy_entity = None  # The type of related entity this actuator acts on

    @property
    def name(self):
        """
        Get the name of the actuator
        :return: a string
        """
        return self.__name

    @property
    def load_profile_act(self):
        """
        Get the profile activities of this actuator
        :return: a LPActivities object
        """
        return self.__load_profile_act

    @load_profile_act.setter
    def load_profile_act(self, lp_act):
        """
        Set the profile activities of this actuator
        :lp: a LPActivities object
        """
        self.__load_profile_act = lp_act


    @property
    def related_entity_type(self):
        """
        Get the profile activities of this actuator
        :return: a String object
        """
        return self.__rel_energy_entity

    @related_entity_type.setter
    def related_entity_type(self, t):
        """
        Set the profile activities of this actuator
        :lp: a String object
        """
        self.__rel_energy_entity = t

    @property
    def action_type(self):
        """
        Get the type of action for this actuator
        :return: a string
        """
        return self.__action_type

    @action_type.setter
    def action_type(self, t):
        """
        Set the type of action for this actuator
        :t: a string
        """
        self.__action_type = t

    @property
    def model_param(self):
        """
        Get the additional param (the model parameters)
        :return: a string
        """
        return self.__additional_param

    def get_forecast(self, time_data):
        """
        Get the forecast of the actuator status for the specified time data
        :param time_data: a tuple (t_0, t_hor, t_step) where:
                - t_0 is the instant corresponding to the first value of the returned vector (in seconds)
                - t_hor is the length of the time period of the returned vector of data
                - t_step is the time interval between two values of the returned vector of data
        :return: a list of numerical values representing the power forecast, corresponding to time_data
        """
        (t_cur, t_hor, t_step) = time_data

        # Load Profile manipulation
        x = []
        y = []

        #TODO: from deterministic to stochastic
        # Go through the lp activities t_start, t_end until reaching t_cur, then build the event-based signal.

        for i in range(self.load_profile_act.size):
            a = self.load_profile_act.get(i)

            t_start = a.mean_start_time
            v_start = a.param['value']
            t_end = t_start + a.mean_duration

            # Start of the activity
            x.append(t_start)
            y.append(v_start)

            # End of the activity
            x.append(t_end)
            y.append(0)

        # Everything before the first activity: 0 !
        if x[0] != 0:
            x = [0] + x
            y = [0] + y

        event_signal = PiecewiseConstantSignal(x, y, end_x=self.load_profile_act.period)  # Init with [x0, 0] couple
        return event_signal.get_interval_val(t_cur, t_cur+t_hor, t_step)

        #return (int(t_hor / t_step) + 1) * [0]

