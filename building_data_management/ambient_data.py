__author__ = 'Olivier Van Cutsem'

from ems_config import *
import csv

DATA_FILE_NAME_LUM_TEMP = 'swissdata_2015-01-01_to_2015-12-31.txt'
DATA_FILE_NAME_PRICE = 'data_elec_price.csv'

DATA_FILE_INDEX_TIME = 4

DATA_FILE_INDEX_TEMP = 1

DATA_FILE_INDEX_IRR_TOT = None
DATA_FILE_INDEX_IRR_DIFF = 2
DATA_FILE_INDEX_IRR_BEAM = 3


class EnvironmentalDataManagement:
    """
    EnvironmentalDataManagement is a class used as a static data structure.
    It loads data from a file for further manipulating data about environmental condition
    """
    def __init__(self, data_file=None):

        self.comf_data = {EMS_ENV_DATA_TEMP: None, EMS_ENV_DATA_LUM: None, EMS_ENV_DATA_IRR: None}
        self.grid_data = {EMS_ENV_DATA_ELEC_PRICE: None}

        if data_file is None:
            data_file = SIMULATION_DATA_FILENAME

        self.loadEnvironmentData(data_file)

    @property
    def environmentTemp(self):

        return self.comf_data[EMS_ENV_DATA_TEMP]

    @property
    def environmentIrr(self):

        return self.comf_data[EMS_ENV_DATA_IRR]

    @property
    def elecPrice(self):

        return self.grid_data[EMS_ENV_DATA_ELEC_PRICE]

    def loadEnvironmentData(self, data_path):
        # TODO: separate the temperature and irradiance data for a modular reading

        ### ENVIRONMENT DATA FROM FILES

        time_v, irr_v, temp_v = EnvironmentalDataManagement.read_env_data(data_path + DATA_FILE_NAME_LUM_TEMP,
                                                                          (DATA_FILE_INDEX_TIME,
                                                                           DATA_FILE_INDEX_TEMP,
                                                                           (DATA_FILE_INDEX_IRR_BEAM,
                                                                            DATA_FILE_INDEX_IRR_DIFF)))

        self.comf_data[EMS_ENV_DATA_TEMP] = dict()
        self.comf_data[EMS_ENV_DATA_TEMP]['t'] = time_v
        self.comf_data[EMS_ENV_DATA_TEMP]['v'] = temp_v

        self.comf_data[EMS_ENV_DATA_IRR] = dict()
        self.comf_data[EMS_ENV_DATA_IRR]['t'] = time_v
        self.comf_data[EMS_ENV_DATA_IRR]['v'] = irr_v

        ### PRICE OF ELECTRICITY
        time_v, prive_v = EnvironmentalDataManagement.read_price_data(data_path + DATA_FILE_NAME_PRICE)

        self.grid_data[EMS_ENV_DATA_ELEC_PRICE] = dict()
        self.grid_data[EMS_ENV_DATA_ELEC_PRICE]['t'] = time_v
        self.grid_data[EMS_ENV_DATA_ELEC_PRICE]['v'] = prive_v

    @staticmethod
    def read_env_data(file_path, index_pack):

        (i_t, i_temp, i_irr) = index_pack

        if type(i_irr) is tuple:  # Irradiance diff and direct
            (i_irr_beam, i_irr_diff) = i_irr

        time_v = []
        irr_v = []
        temp_v = []
        try:
            # read the data from the file
            ref_time = None
            with open(file_path, 'rb') as csvfile:
                csvreader = csv.reader(csvfile, delimiter=',', quotechar='|')
                for row in csvreader:
                    try:
                        t = float(row[i_t])

                        temp = float(row[i_temp])  # Temperature

                        if type(i_irr) is tuple:  # Irradiance diff and direct
                            irr = (float(row[i_irr_beam]), float(row[i_irr_diff]))
                        else:
                            irr = float(row[i_irr])

                        if ref_time is None:
                            ref_time = t

                        time_v.append(t-ref_time)
                        irr_v.append(irr)
                        temp_v.append(temp)
                    except:
                        pass
        except OSError:
            print('EMS cannot open file {0}'.format(file_path))

        return time_v, irr_v, temp_v

    @staticmethod
    def read_price_data(file_path):

        i_t = 0
        i_p = 1
        time_v = []
        price_v = []
        try:
            # Read the data from the file
            ref_time = None
            with open(file_path, 'rb') as csvfile:
                csvreader = csv.reader(csvfile, delimiter=',', quotechar='|')
                for row in csvreader:
                    try:
                        t = float(row[i_t])
                        price = float(row[i_p])

                        if ref_time is None:
                            ref_time = t

                        time_v.append(t - ref_time)
                        price_v.append(price)
                    except:
                        pass
        except OSError:
            print('EMS cannot open file {0}'.format(file_path))

        return time_v, price_v