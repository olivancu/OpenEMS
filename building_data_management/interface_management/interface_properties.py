__author__ = 'Olivier Van Cutsem'

class InterfaceThermalProperty:
    RESISTANCE_KEY = 'r'
    CAPACITY_KEY = 'c'

    def __init__(self, thermal_param):

        list_r = thermal_param[InterfaceThermalProperty.RESISTANCE_KEY]
        list_c = thermal_param[InterfaceThermalProperty.CAPACITY_KEY]

        assert(len(list_r) == (len(list_c) + 1))

        self.__modelOrder = len(list_c)

        self.__resistiveParam = list_r
        self.__capacitiveParam = list_c

    @property
    def resistiveParameters(self):
        return self.__resistiveParam

    @property
    def capacitiveParam(self):
        return self.__capacitiveParam

    @property
    def orderModel(self):
        return self.__modelOrder

class InterfaceLuminosityProperty:

    def __init__(self):
        pass