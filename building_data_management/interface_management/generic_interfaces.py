__author__ = 'Olivier Van Cutsem'

from building_data_management.interface_management.interface_properties import *

EMS_INTERFACE_WINDOW = 'window_interface'
EMS_INTERFACE_DOOR = 'door_interface'
EMS_INTERFACE_WALL = 'wall_interface'

class BasicInterface(object):

    def __init__(self, s, t, thProp=None, lumProp=None):

        self.__surface = s
        self.__thickness = t

        # Thermal property of this interface
        self.__thermalProp = None

        if thProp is not None:
            self.__thermalProp = InterfaceThermalProperty(thProp)

        # Luminosity property of this interface
        self.__luminosityProp = None
        if lumProp is not None:
            self.__thermalProp = InterfaceLuminosityProperty(lumProp)

    @property
    def thermalProp(self):
        return self.__thermalProp

    @thermalProp.setter
    def thermalProp(self, thProp):
        self.__thermalProp = thProp

    @property
    def luminosityProp(self):
        return self.__thermalProp

    @luminosityProp.setter
    def luminosityProp(self, lumProp):
        self.__luminosityProp = lumProp

    @property
    def surface(self):
        return self.__surface

    @property
    def thickness(self):
        return self.__thickness

### SPECIFIC INTERFACES ###


class WallInterface(BasicInterface):
    """
    Basic one, no actuator and no luminosity property
    """
    def __init__(self, s, t, thProp):
        super(WallInterface, self).__init__(s, t, thProp, None)

    @property
    def luminosityProp(self):
        return None

    @property
    def isOpen(self):
        return False


class WindowInterface(BasicInterface):
    """
    A window interface, having thermal and luminosity property
    May have two actuator linked:
     - for the window to open/close
     - for the blind
    """
    def __init__(self, s, t, thProp, lumProp, irr_to_heat):
        super(WindowInterface, self).__init__(s, t, thProp, lumProp)

        self.__luminosityProp = None

        # The list of actuators IDs
        self.__actuators = {'OPENING': None, 'BLIND': None}
        self.__state = {'OPENING': False, 'BLIND': 0}  # TO BE DEFINED !

        # The conversion model from irradiance to heat
        self.__irrTransfer = irr_to_heat  # Todo !

    @property
    def openingActuator(self):
        return self.__actuators['OPENING']

    @property
    def isOpen(self):
        return self.__state['OPENING'] == True

    @property
    def blindPosition(self):
        return self.__state['BLIND']

    @openingActuator.setter
    def openingActuator(self, id):
        self.__actuators['OPENING'] = id

    @property
    def blindActuator(self):
        return self.__actuators['BLIND']

    @blindActuator.setter
    def blindActuator(self, id):
        self.__actuators['BLIND'] = id


class DoorInterface(WallInterface):
    """
    Basic one, no actuator and no luminosity property
    """
    def __init__(self, s, t, thProp):
        super(DoorInterface, self).__init__(s, t, thProp)

        # The list of actuators IDs
        self.__actuator = None

    @property
    def openingActuator(self):
        return self.__actuator

    @openingActuator.setter
    def openingActuator(self, id):
        self.__actuator = id