__author__ = 'Olivier Van Cutsem'

from building_data_management.energyrelated_data import BuildingEnergyRelatedEntityData
from building_data_management.irradiance_toolbox.irradiance import tilted_surface_irradiance

#TODO: merge with vEngine constants ...
# Todo: generic BuildingGenerationData and specialized PVpanel class
VE_GEN_REF_PARAM = 'reference_param'
VE_GEN_REF_PARAM_IRR = 'ref_irradiance'
VE_GEN_REF_PARAM_TEMP = 'ref_temperature'
VE_GEN_REF_PARAM_TEMP_NTC = 'temperature_normal_cond'

VE_GEN_PV_PARAM = 'pv_param'
VE_GEN_PV_PARAM_STD_VOLTAGE = "std_cond_voltage"
VE_GEN_PV_PARAM_STD_CURRENT_TEMP_COEFF = "current_temperature_coeff"
VE_GEN_PV_PARAM_STD_CURRENT = "std_cond_current"
VE_GEN_PV_PARAM_STD_VOLTAGE_TEMP_COEFF = "voltage_temperature_coeff"
VE_GEN_PV_PARAM_NB_CELLS = 'nb_cells'

VE_GEN_PV_PARAM_TILTED_ANGLE = 'tilted_surface'
VE_GEN_PV_PARAM_AZIMUTH_ANGLE = 'azimuth_angle'


class BuildingGenerationData(BuildingEnergyRelatedEntityData):
    """
    A class that stores the data structure of any energy generation system
    """

    def __init__(self, name, nom_power, related_sensors):
        """
        Constructor
        :param name: a string representing the name of the energy generation system
        :param nom_power: a numerical value representing the nominal power of the energy generation system
        :param related_sensors: a dictionary listing the sensors linked to this energy generation system
        """
        super(BuildingGenerationData, self).__init__(related_sensors=related_sensors, name=name)

        self.__name = name
        self.__nominal_power = nom_power

        # Generic data
        self.__generation_type = None

        # Model
        self.__model_parameters = None

    @property
    def nom_power(self):
        """
        Get the nominal generation power of this energy generation system
        :return: a numerical value representing the nominal generation power of this energy generation system
        """
        return self.__nominal_power

    @nom_power.setter
    def nom_power(self, n):
        """
        Set the nominal generation power of this energy generation system
        :v: a numerical value representing the nominal generation power of this energy generation system
        """
        self.__nominal_power = n

    @property
    def model_parameters(self):
        """
        Get the model parameters of this energy generation system
        :return: a structured dictionary representing the model parameter of this energy generation system
        """
        return self.__model_parameters

    @model_parameters.setter
    def model_parameters(self, m):
        """
        Set the model parameters of this energy generation system
        :m: a structured dictionary representing the model parameter of this energy generation system
        """
        self.__model_parameters = m

    def generation_forecast(self, time_data, temp_pred, irr_pred, lum_pred, coord_info):
        """
        Get the forecast of the generated power for the time interval time_data, based on prediction for temperature, irradiance and luminosity.
        :param time_data: a tuple (t_0, t_hor, t_step) where:
                - t_0 is the instant corresponding to the first value of the returned vector (in seconds)
                - t_hor is the length of the time period of the returned vector of data
                - t_step is the time interval between two values of the returned vector of data
        :param temp_pred: a list of numerical values representing temperatures
        :param irr_pred: a list of numerical values representing irradiance
        :param lum_pred: a list of numerical values representing luminosity
        :param coord_info: a tuple (LAT, LONG)
        :return: a list of numerical values representing the power forecast, corresponding to time_data
        """
        (t_cur, t_hor, t_step) = time_data

        # From the model, generate each [t, v]
        nb_pts = int(t_hor/t_step)
        ret = nb_pts * [0]

        for i in range(0, nb_pts):
            ret[i] = self.get_model_value(temp=temp_pred[i], irr=irr_pred[i], time_sec=t_cur, coord_info=coord_info)

        return ret

    ##### ---- Model Speficic ----- ######
    def get_model_value(self, temp, irr, time_sec, coord_info):
        """
        Compute the power that the PV panel should generate at a given temperature and irradiance
        :param temp: a numerical value representing the PV panel ambient temperature
        :param irr: a numerical value representing the PV panel incoming irradiance
        :param time_sec: the number of seconds since the beginning of the year
        :return: a numerical value representing the generated power according to the model
        """
        #ref param
        t_ref_ntc = self.model_parameters[VE_GEN_REF_PARAM][VE_GEN_REF_PARAM_TEMP_NTC]
        irr_ref = self.model_parameters[VE_GEN_REF_PARAM][VE_GEN_REF_PARAM_IRR]
        t_ref = self.model_parameters[VE_GEN_REF_PARAM][VE_GEN_REF_PARAM_TEMP]

        #model param
        u_p = self.model_parameters[VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_STD_VOLTAGE]
        i_p = self.model_parameters[VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_STD_CURRENT]
        di_p_dT = self.model_parameters[VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_STD_CURRENT_TEMP_COEFF]
        du_p_dT = self.model_parameters[VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_STD_VOLTAGE_TEMP_COEFF]
        nb_cells = self.model_parameters[VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_NB_CELLS]

        # Physical angles
        tilted_surface = self.__model_parameters[VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_TILTED_ANGLE]
        azimuth_angle = self.__model_parameters[VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_AZIMUTH_ANGLE]

        # Model of P generated
        if type(irr) is tuple:
            time_info = (time_sec % (24*3600), time_sec % 3600, time_sec % 60)
            irr = tilted_surface_irradiance(time_info, irr, tilted_surface, coord_info, azimuth_angle)

        k = irr / irr_ref

        T_cell = temp + k * (t_ref_ntc - 20)
        u_mppt = u_p * (1 + du_p_dT * (T_cell - t_ref))
        i_mppt = i_p * k * (1 + di_p_dT * (T_cell - t_ref))

        #  The power is given to the system, so negative
        return min(-nb_cells * u_mppt * i_mppt, 0)
