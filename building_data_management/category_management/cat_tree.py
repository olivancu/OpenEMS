__author__ = 'Olivier Van Cutsem'

DICT_CREATION_LABEL_LIST = 'list-label'

class CategoryTree(object):
    """
    TODO: describe how to use it
    """

    def __init__(self, category_struct=None):

        self.category_set = {}

        if type(category_struct) is dict:
            self.category_set = self.create_set(category_struct)

    def insert(self, path, entity_id, value=None):
        self.set_value(path, entity_id, value)

    def set(self, entity_id, v):
        [o, path] = self.find_in_set(entity_id, self.category_set)
        self.set_value(path, entity_id, v)

    def get(self, entity_id, cat=None):
        cat_set = self.category_set
        if cat is not None:
            cat_set = self.get_sub_tree(cat)

        [o, path] = self.find_in_set(entity_id, cat_set)
        return o, path

    def getEntitiesValuesList(self, label=None):

        root = self.get_sub_tree(label)

        if root != {}:  # Return all the objects
            return self.list_of_item(root)
        else:
            return []

    def getEntitiesIdList(self, label):

        root = self.get_sub_tree(label)

        if root != {}:  # Return all the objects
            return self.list_of_item(root, val_select=False)
        else:
            return []

    ##################################
    ### Dictionary manipulation ###
    ###############################

    # Normally private methods #

    def create_set(self, cat_struct):
        """
        :param cat_struct: a dictionary describing the categories.
        Each key DICT_CREATION_LABEL_LIST indicates the sub-categories of each category
        :return: a dictionary ready to receive data in each category

        Example:
            cat_struct = {['cars' 'motorcycle' 'bikes'], 'cars': ['diesel' 'ev']}
            return: {'cars': {'diesel': {}, 'ev': {}}, 'motorcycle': {}, 'bikes': {}}
        """
        cat_dict = {}

        # if the label that describes the list of sub-cat is found, keep digging recursively
        if DICT_CREATION_LABEL_LIST in cat_struct:
            list_cat = cat_struct[DICT_CREATION_LABEL_LIST]  # list of the sub-categories
            for k in list_cat:
                cat_dict[k] = {}
                if k in cat_struct.keys():
                    cat_dict[k] = self.create_set(cat_struct[k])

        return cat_dict

    def find_in_set(self, id, set):

        if set is None:
            return None, []

        # Either it's a leaf, we check if it contains the value
        if self.isLeaf(set):
            if id in set:  # OK you're looking after me !
                return set[id], []
            else:
                return None, []

        # OR look to all of the children of the current category

        for k in set.keys():
            [obj, path] = self.find_in_set(id, set[k])  # OK we found the item in this category
            if obj is not None:
                return obj, (list(k) + path)

        return None, []

    def isLeaf(self, set):
        """
        A leaf is defined as a dictionary whose values are not dictionary and hence contains values
        :param set:
        :return:
        """

        l = set.values()

        if l is None or l == [] or type(l[0]) is not dict:
            return True
        else:
            return False

    def get_value(self, path):

        cur_dict = self.category_set

        for k in path:
            if k in cur_dict:
                cur_dict = cur_dict[k]
            else:
                return None

        return cur_dict

    def set_value(self, path, id, v):
        """
        :param path:
        :param id:
        :param v:
        :return: set[path][id] = v
        """
        cur_dict = self.category_set
        for k in path:
            if k in cur_dict:
                cur_dict = cur_dict[k]
            else:
                return None

        cur_dict[id] = v
        return v

    def get_sub_tree(self, path):

        if path is None or path == []:
            return self.category_set

        if type(path) is not list:
            path = [path]

        sub_tree = self.category_set
        for k in path:
            if k not in sub_tree.keys():
                return {}

            sub_tree = sub_tree[k]

        return sub_tree

    def list_of_item(self, set, val_select=True):

        # Either it's a leaf, just return the list of values
        if self.isLeaf(set):
            if val_select:
                return set.values()
            else:
                return set.keys()

        # OR dig into the children of each category
        l_item = []
        for k in set.keys():
            l_item += self.list_of_item(set[k], val_select)

        return l_item

