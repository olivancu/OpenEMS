__author__ = 'Olivier Van Cutsem'

from bmsinterface.bms_interface_config import *
from building_data_management.comfort_data import BuildingComfortData
from building_data_management.actuator_data import BuildingActuatorData
from building_data_management.interface_management.generic_interfaces import EMS_INTERFACE_WINDOW, EMS_INTERFACE_DOOR, EMS_INTERFACE_WALL
from building_data_management.interface_management.generic_interfaces import WindowInterface, WallInterface, DoorInterface
from ems_config import EMS_OUTSIDE_ROOM_NAME, EMS_ROOM_NATURE_HUMAN_SPACE, EMS_ROOM_NATURE_WATER_ENERGY_STORAGE, EMS_ROOM_NATURE_ENVIRONMENT, EMS_ROOM_TYPE_REGULAR_ROOM
from building_data_management.load_data import EMS_LOAD_DEVICE_TYPE_AC, EMS_LOAD_DEVICE_TYPE_ELEC_HEATER, EMS_LOAD_DEVICE_TYPE_HP
from building_data_management.irradiance_toolbox.irradiance import building_thermal_effect, BLIND_POSITION_OPEN

from building_data_management.signal_and_constraint_management.signal_data import SampledSignal

from building_data_management.ambient_data import EnvironmentalDataManagement  #TODO: remove this when MISSING_DATA is not needed anymore

# Useful thermal constant

VTEMPSENSOR_TEMP_STC = 273.15

# Air density and heat capacity
RHO_AIR_STC = 1.292
AIR_HEAT_CAPA = 1004

# Water density and heat capacity
RHO_WATER_STC = 1000.0
WATER_HEAT_CAPA = 4185.5

# Irradiance building influence
WINDOW_WALL_TILT = 90
WINDOW_KL = 0.03  # exctinction coeff. times glazing thickness (most common is glazing thickness of 20mmm
WINDOW_NG = 1.53  # refractive index (main glass are between 1.52 and 1.54 ...)
WINDOW_GLAZING = 2  # 1 for single glazing and 2 for double
WINDOW_DIRECT_BLOCK_TRANSM = 0.07  # Radiation transmission coefficient when the blinds block direct sunlight

class BuildingRoomData():
    """
    This class stores information about a single room in the building
    """

    TYPE_COMFORT_LIST = [EMS_COMFORT_temperature, EMS_COMFORT_luminosity, EMS_COMFORT_irradiance, EMS_COMFORT_presence, EMS_COMFORT_humidity]

    def __init__(self, name, dim=None, nature_type=None):
        """
        Constructor
        :param name: a string indicating the name of the room
        """

        # General room data
        self.__name = name
        self.__surface = None
        self.__volume = None

        if dim is not None:
            (s, v) = dim
            self.__surface = s
            self.__volume = v

        # Nature and type of the room
        self.__nature = EMS_ROOM_NATURE_HUMAN_SPACE
        self.__type = EMS_ROOM_TYPE_REGULAR_ROOM
        if nature_type is not None:
            (r_nature, r_type) = nature_type
            self.__nature = r_nature
            self.__type = r_type

        # Sensors of this room
        self.__comfort = {EMS_COMFORT_temperature: None,
                          EMS_COMFORT_luminosity: None,
                          EMS_COMFORT_presence: None,
                          EMS_COMFORT_irradiance: None,
                          EMS_COMFORT_humidity: None}

        # Interface with other rooms
        self.__interface = dict()

        # Actuator in this room: id->obj
        self.__actuators = dict()

    ### INIT ###

    def initSensordata(self, type_measure, current_value, sensor_id):
        """
        Fill the empty structures with the BMS data
        :param room_sensors: a formatted dictionary containing all the sensors of this room and their data
        :return: /
        """
        self.__comfort[type_measure] = BuildingComfortData(type_measure, None, current_value)
        self.__comfort[type_measure].sensor = sensor_id

    def setInterface(self, neighborID, interface):
        """
        Insert or modify an interface that this current room has with a neighbor room
        :param neighborID: an integer value representing the ID of the interfaced room
        :param interface: an Object heritated from BasicInterface, describing the properties of the interface
        :param multi_interface: if True, add this interface in addition to others
        :return: /
        """

        if neighborID in self.__interface:
            self.__interface[neighborID].append(interface)
        else:
            self.__interface[neighborID] = [interface]

    ### SENSORS & ACTUATORS manipulation

    def get_comfort_type(self, sensor_id):
        """
        Identify the comfort linked to the given sensor
        :param sensor_id: an integer representing the unique ID of the sensor
        :return: a string representing the type of comfort associated to sensor_id. None if not found
        """
        for type in self.__comfort.keys():
            if sensor_id == self.__comfort[type].sensor:  # the type of sensor has been identified
                return type

        return None

    def get_reference_value(self, type_comf):

        if self.__comfort[type_comf] is None:
            return None

        return self.__comfort[type_comf].ref_value

    def get_forecast(self, type_comf, t_data, data_obj=None):
        """
        If this room data structure has a vector forecast for the "type_comf" in the period t_data, return the
        appropriate vector
        :param type_comf: a string representing the type of comfort whose forecast must be known
        :param t_data: a tuple (t_0, t_hor, t_step) where:
                - t_0 is the instant corresponding to the first value of the returned vector (in seconds)
                - t_hor is the length of the time period of the returned vector of data
                - t_step is the time interval between two values of the returned vector of data
                that represent the time period of the requested forecast
        :return: a list of numerical values representing the forecast for each time instant described by t_data
        """

        (t_cur, t_hor, t_step) = t_data

        if data_obj is None:
            data_obj = EnvironmentalDataManagement()

        #TODO: CHANGE THE LOADING VALUES !
        if type_comf == EMS_COMFORT_temperature and self.name == EMS_OUTSIDE_ROOM_NAME:
            data_raw = data_obj.environmentTemp

            sig_object = SampledSignal(data_raw['t'], data_raw['v'])

            return sig_object.resample(t_step, t_cur, t_cur + t_hor)
        else:
            return (int(t_hor/t_step)+1) * [0]

    def set_comfort_value(self, value, sensor_id=None, type_comf=None):
        """
        Set the comfort value linked to a specific sensor or type of comfort.
        :param value : the new comfort value
        :param sensor_id: [optional] an integer value representing the ID of the sensor linked to the comfort
        :param type_comf: [optional] a string representing the type of comfort
        :return: sensor_id and type_comf cannot be both undefined
        """

        assert(not(sensor_id is None and type_comf is None))  # cannot both be undefined

        if type_comf is None:
            type_comf = self.get_comfort_type(sensor_id)

        if type_comf is not None:  # Now the value can be updated
            self.__comfort[type_comf].comfort_value = value

    def hasSensor(self, sensor_id, type_comf=None):
        """
        return True if this room contains the sensor_id - corresponding to an optional type_comf
        :param sensor_id: an integer value representing the sensor ID
        :param type_comf: a string representing the type of comfort
        :return: a boolean value, True if this room holds the sensor ID "sensor_id", that corresponds to an optional type of comfort "type_comf"
        """

        if type_comf is None:
            list_comfort = self.__comfort.keys()
        else:
            list_comfort = [type_comf]

        for type in list_comfort:
            if self.__comfort[type] is not None and sensor_id == self.__comfort[type].sensor:  # the type of sensor has been identified
                return True

        return False

    def set_comfort_constraint(self, type_comf, constr):
        """
        Set the comfort constraints linked to a specific type of comfort
        :param type_comf: a string referring to the type of comfort
        :param constr: a Constraint object that describes the comfort constraint to set on the comfort
        :return: /
        """

        if self.__comfort[type_comf] is not None:
            self.__comfort[type_comf].comfort_constraints = constr

    def set_reference_value(self, type_comf, val):

        if self.__comfort[type_comf] is not None:
            self.__comfort[type_comf].ref_value = val

    def has_interface_with(self, neighbor, type_interface=None):
        """
        Indicate whether this room has an interface with a specified room
        :param neighbor: an integer value representing the ID of the specified room
        :param type_interface: [optional] a string indicating the type of interface to look for
        :return: a boolean value, true if this room such an interface with "neighbor"
        """
        if neighbor in self.__interface.keys():
            if type_interface is None:
                return True
            else:
                if type_interface == EMS_INTERFACE_WINDOW and type(self.__interface[neighbor]) is WindowInterface:
                    return True
                elif type_interface == EMS_INTERFACE_DOOR and type(self.__interface[neighbor]) is DoorInterface:
                    return True
                elif type_interface == EMS_INTERFACE_WALL and type(self.__interface[neighbor]) is WallInterface:
                    return True
                else:
                    return False
        else:
            return False

    def set_actuators(self, actuator_data):
        for a_id, a_data in actuator_data.items():
            self.__actuators[a_id] = BuildingActuatorData(name=a_data[DB_KEY_ACTUATOR_NAME],
                                                          lp=a_data[DB_KEY_ACTUATOR_LP],
                                                          additional_param=a_data[DB_KEY_ACTUATOR_ADD_PARAM])

            self.__actuators[a_id].action_type = a_data[DB_KEY_ACTUATOR_ACTION_TYPE]
            self.__actuators[a_id].related_entity_type = a_data[DB_KEY_ENERGY_ENTITY_RELATED_TYPE]

    def get_actuators(self, act_type, entity_rel_type):
        ret = []
        for k, v in self.__actuators.items():
            if v.action_type == act_type and v.related_entity_type == entity_rel_type:
                ret.append(v)

        return ret
    ### GETTER & SETTER ###

    @property
    def name(self):
        """
        Get the name of this room
        :return: a string referring the name of this room
        """
        return self.__name

    @name.setter
    def name(self, v):

        """
        Set the name of this room
        :param v: a string referring the name of this room
        """
        self.__name = v

    @property
    def volume(self):
        """
        Get the volume of this room
        :return: a numerical value referring the volume of this room
        """
        return self.__volume

    @volume.setter
    def volume(self, v):
        """
        Set the volume of this room
        :param v: a numerical value referring the volume of this room
        """
        self.__volume = v

    @property
    def surface(self):
        """
        Get the surface of this room
        :return: a numerical value referring the surface of this room
        """
        return self.__surface

    @surface.setter
    def surface(self, v):
        """
        Set the surface of this room
        :param v: a numerical value referring the surface of this room
        """
        self.__surface = v

    @property
    def comfort(self):
        """
        Get the comfort description of this room
        :return: a formatted dictionary that holds BuildingComfortData objects for each type of comfort
        """
        return self.__comfort

    @property
    def nature(self):
        """
        Get the nature of this room
        :return: a string
        """
        return self.__nature

    @property
    def type(self):
        """
        Get the type of this room
        :return: a string
        """
        return self.__type

    @property
    def interfaces(self):
        """
        Get the interfaces description of this room with the other rooms
        :return: a dictionary that hold pairs (n_id, interf) where
                    - n_id is an integer value that refers to the neighbor room ID
                    - interf is a list of objects inherited from BasicInterface
        """
        return self.__interface

    @property
    def thermal_capa(self):
        """
        Get the thermal capacity of this room, as a function of the current temperature
        :return: a numerical value that represents the thermal capacity of this room
        """

        if self.nature == EMS_ROOM_NATURE_HUMAN_SPACE:  # Air filled
            temp = self.get_comfort_value(EMS_COMFORT_temperature) + VTEMPSENSOR_TEMP_STC  # in K
            rho = RHO_AIR_STC * VTEMPSENSOR_TEMP_STC / temp
            specific_hc = AIR_HEAT_CAPA
        elif self.nature == EMS_ROOM_NATURE_WATER_ENERGY_STORAGE:  # Water filled
            rho = RHO_WATER_STC
            specific_hc = WATER_HEAT_CAPA
        else:  # Environment or unknown
            return None

        return rho * specific_hc * self.volume

    def get_comfort_value(self, type_comfort):
        """
        Get the current value of the comfort of a specified type
        :param type_comfort: a string representing the type of comfort of interest
        :return: a numerical value that represents the comfort value of type "type_comfort"
        """

        if type_comfort not in self.__comfort or self.__comfort[type_comfort] is None:
            return None

        return self.__comfort[type_comfort].comfort_value

    def get_comfort_constraint(self, type_comfort):
        """
        Get the constraints on the comfort of a specified type
        :param type_comfort: a string representing the type of comfort of interest
        :return: a Constraint object (or sub-class) that represents the comfort constraints on type "type_comfort"
        """
        if type_comfort not in self.__comfort or self.__comfort[type_comfort] is None:
            return None

        return self.__comfort[type_comfort].comfort_constraints

    def get_comfort_loads(self, type_comfort):
        """
        Get the load IDs who are impacting a specific type of comfort
        :param type_comfort: a string representing the type of comfort of interest
        :return: a list of load IDs
        """

        if type_comfort not in self.__comfort or self.__comfort[type_comfort] is None:
            return []

        return self.__comfort[type_comfort].loads

    def get_solar_heat_gain(self, t_data, irr_pred, coord_info):
        """
        Compute the solar heat gain for a specified time period, given outside irradiance for that period
        :param t_data: a tuple (t_0, t_hor, t_step) where:
                - t_0 is the instant corresponding to the first value of the returned vector (in seconds)
                - t_hor is the length of the time period of the returned vector of data
                - t_step is the time interval between two values of the returned vector of data
                that represent the time period of the requested forecast
        :param irr_pred: a tuple (irr_beam, irr_diff) containing list of numerical values representing the irradiance signal
        :param coord_info: a tuple (LAT, LONG)
        :return: a vector containing power heat gain in W
        """

        # consider a generic model with a window at SOUTH, EAST and WEST
        windows_angles = [90, 180, 270]
        window_generic_size = 1.5  # square meter

        (t_cur, t_hor, t_step) = t_data

        irr_influence = int(t_hor/t_step) * [0]

        for w_az in windows_angles:
            i = 0
            for t in range(t_cur, t_cur+t_hor, t_step):
                time_info = (t % (24*3600), t % 3600, t % 60)
                irr_influence[i] += building_thermal_effect(time_info, irr_pred, coord_info,
                                                            WINDOW_KL, WINDOW_NG, w_az, WINDOW_WALL_TILT, WINDOW_GLAZING,
                                                            BLIND_POSITION_OPEN, WINDOW_DIRECT_BLOCK_TRANSM)
                i += 1

        return irr_influence

    def has_forecast(self, t_data, type_comfort):
        """
        Return TRUE if this room data structure has forecast data concerning "type_comfort", for the period indicated by "t_data"
        :param t_data: a tuple (t_0, t_hor, t_step) where:
                - t_0 is the instant corresponding to the first value of the returned vector (in seconds)
                - t_hor is the length of the time period of the returned vector of data
                - t_step is the time interval between two values of the returned vector of data
                that represent the time period of the requested forecast
        :param type_comfort: a string representing the type of comfort of interest
        :return: a boolean value
        """

        # TODO: TEMPORARY :D !!

        return self.name == EMS_OUTSIDE_ROOM_NAME

    def has_thermalsimu_data(self):
        """
        Return TRUE if this room holds necessary information for simulating its thermal evolution
        :return: a boolean value
        """

        # TODO: more complete conditions?
        # If there is no indication concerning the room volume, this room is considered out of the simulation model
        return self.volume > 0 and len(self.__interface.keys()) > 0

