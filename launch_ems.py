__author__ = 'Olivier Van Cutsem'

from multiprocessing import Queue

from ems_analysis.main_analysis import EMS_ANALYZER
from rtevents_interface.rtevents_interf import RTEventInterface
from ems_main import EnergyManagementSystem
from emscore.mpc import EMS_MPC
from emscore.ems_test import EMS_simple

########### LAUNCHING THE THREADS ###################


def ems_launcher(unit=None,
                 ems_dt=None,
                 simu_day=None, simu_data_filename=None,
                 gui_stream=None):

    bms_stream_rtevents = Queue()
    ems_stream_rtevents = Queue()
    if gui_stream is None:
        ems_stream_gui = Queue()
    else:
        ems_stream_gui = gui_stream

    rtevents_interf = RTEventInterface(bms_stream_rtevents, ems_stream_rtevents, unit)  # RT Interface

    ems_queues = (bms_stream_rtevents, ems_stream_rtevents, ems_stream_gui)
    ems_core = EMS_simple(queues=ems_queues, unit=unit, ems_time_step=ems_dt, simu_starting_day=simu_day, data_path=simu_data_filename)  # The core of the EMS logic

    # Start the thread

    rtevents_interf.start()
    ems_core.start()

    rtevents_interf.join()
    ems_core.join()


if __name__ == '__main__':
    ems_launcher()
