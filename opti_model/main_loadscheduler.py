__author__ = 'Olivier Van Cutsem'

import numpy
from abc import abstractmethod


class LoadScheduler(object):
    """
    This class stores the loads that must be scheduled and the one that are scheduled but waiting to be activated.
    It also offers methods to manipulate those structures.
    """

    def __init__(self):

        # Loads to be scheduled
        """
        a list of pair (id, obj) where
         - id is the unique identifier of the shiftable load
         - obj is the reference to a BuildingShiftLoadData object, representing the shiftable load
        """
        self._list_shift_loads = []

        # Parameters for the scheduling strategies
        self._scheduling_param = dict()

    def initFromEMS(self, list_deferrable_loads):

        self._list_shift_loads = list_deferrable_loads

    def get_scheduled_loads(self):
        """
        Get the list of loads that have been scheduled and their scheduling details
        :return: a dictionary of pairs (id, t_start) where:
                    - id: stands for the ID of the deferrable load
                    - t_start: is the scheduled starting time of the load
        """

        scheduled_loads = {}
        for (l_id, l_obj) in self._list_shift_loads:
            if l_obj.get_scheduled_time() is not None:
                scheduled_loads[l_id] = l_obj.get_scheduled_time()

        return scheduled_loads

    def get_loads_to_trigger(self, t, t_e=None):
        """
        Return to list of loads that must be trigger:
         - at time t (or before)
         - or in the interval [t;t_e] if specified
        :param t: a float representing the beginning time (in seconds) of the interval
        :param t_e: [optional] a float representing the ending time (in seconds) of the interval
        :return: a list of ID of the corresponding deferrable loads to be trigger in the specified interval
        """
        if t_e is None:
            l = [l_id for l_id, l_obj in self._list_shift_loads
                 if l_obj.get_scheduled_time() is not None and l_obj.get_scheduled_time() <= t]
        else:
            l = [l_id for l_id, l_obj in self._list_shift_loads
                 if l_obj.get_scheduled_time() is not None and t <= l_obj.get_scheduled_time() <= t_e]

        # TODO: removed only when the loads confirms ! move it outside the function !
        for l_id, l_obj in self._list_shift_loads:
            if l_id in l:
                l_obj.reset_scheduled_time()
                l_obj.set_triggered_time(t)

        return l

                                    ##################################
                                ####### LOAD SCHEDULING STRATEGIES ######
                                    ##################################

    def set_load_scheduling_parameter(self, param, value):
        """
        This method allows to modify a parameter used in the load scheduling strategies
        :param param: a string standing for the key of the parameter to change
        :param value: the new value the parameter "param" must take
        :return: /
        """

        pass

    @abstractmethod
    def schedule_loads(self, time_data, elec_price, max_power):
        """
        This method select a starting time of the load to be scheduled, contained in self._loads_to_schedule and store
        the information in self._scheduled_loads.
        :param time_data: a tuple (t_0, t_hor, t_step) where:
                - t_0 is the instant corresponding to the first value of the returned vector (in seconds)
                - t_hor is the length of the time period of the returned vector of data
                - t_step is the time interval between two values of the returned vector of data
        :param elec_price: a list of numerical values representing the price of electricity at each sampling instant.
        The corresponding time instant span from 0 to horizon.
        :param max_power: a list of numerical values representing the maximum consumption power at each sampling instant.
        The corresponding time instant span from 0 to horizon.
        :return: /
        """
        pass


class DummyLoadScheduler(LoadScheduler):

    def __init__(self):
        super(DummyLoadScheduler, self).__init__()

    def schedule_loads(self, time_data, elec_price, max_power):
        (t_cur, hor, dt) = time_data

        for (load_id, load_obj) in self._list_shift_loads:

            if load_obj.get_scheduled_time() is None:

                min_t_sched = load_obj.get_min_starting_time()
                max_t_sched = load_obj.get_max_starting_time()
                schedule_time = min_t_sched # + numpy.random.random_sample() * (max_t_sched-min_t_sched)
                load_obj.set_scheduled_time(t_cur+schedule_time)
