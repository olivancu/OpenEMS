__author__ = 'Olivier Van Cutsem'


import time

import cvxpy
import numpy as np
import math
import control

from building_data_management.category_management.category_config import *
from building_data_management.interface_management.generic_interfaces import EMS_INTERFACE_WINDOW
from ems_config import *

from building_data_management.room_data import RHO_WATER_STC, WATER_HEAT_CAPA
from building_data_management.load_data import EMS_LOAD_DEVICE_TYPE_HP, EMS_LOAD_DEVICE_TYPE_AC
from building_data_management.actuator_data import ACTUATOR_TYPE_ACTION_DIMMER, VE_VOPENER_VALUE_WHEN_FULLY_OPEN

from main_loadscheduler import LoadScheduler
from energy_planning import EnergyPlanning

# KEYS PARAM defines:

LINOPTI_PARAM_KEY_ECONOMIC_WEIGHT = 'economic_coeff'
LINOPTI_PARAM_KEY_PEAK_WEIGHT = 'minpeak_coeff'


class LinearOptiBuildingModel(object):
    """
    A class that manipulates the linear model of the building components:
     - Thermal simulation of air and water temperature variation
     - Hot Water tank temperature
     - Battery and PHEV
     - Deferrable loads
    """

    def __init__(self):

        # All the building data
        self.bdms = None

        # The class automatically infers the amount of entities for each type of category
        # Room classification for air & water temperature flux

        self.list_room_active_simu = []  # the one that have a RC model
        self.list_room_fix_simu = []  # the one that have a forecast

        # WaterTank: remove the rooms classified as "water tank" from the linear RC model
        self.map_watertank_room = {}

        # Deferrable loads
        self.list_loads_def = []

        # Thermal loads
        self.list_space_hvac = []
        self.list_waterheater = []

        # RC thermal simulation
        self.nb_interf_nodes = None

        # Batteries
        self.list_battery = []

        # The load scheduler object for easy manipulation
        self.load_scheduler = LoadScheduler()

    def get_energy_planning(self, time_data, elec_price, param):
        """
        Compute the energy planning for the day. This is the result of the following optimization problem:

        :param time_data:
        :param elec_price:
        :param param:
        :return: an object of type EnergyPlanning
        """

        # Return an object EnergyPlanning
        planning = EnergyPlanning()

        # The parameter for the objective function
        obj_fct_param = {LINOPTI_PARAM_KEY_ECONOMIC_WEIGHT: 0, LINOPTI_PARAM_KEY_PEAK_WEIGHT: 0}
        if param is not None:
            for k, v in param.keys():
                obj_fct_param[k] = v

        # TODO

        return planning

    def initFromEMS(self, building_data):

        self.bdms = building_data

        # The class automatically infers the amount of entities for each type of category

        # Room classification for air & water temperature flux
        for r_id in self.bdms.room_ids_list:
            if self.bdms.room(r_id).type != EMS_ROOM_TYPE_WATER_TANK:
                # Don't take into account "spaces" that have forecast OR whose type is a Water Tank
                t_pack_fake = (0, 3600, 3600)
                if not (self.bdms.room(r_id).has_forecast(t_pack_fake, EMS_COMFORT_temperature)):
                    self.list_room_active_simu.append(r_id)  # Active in the simu: temperature to be simulated
                else:
                    self.list_room_fix_simu.append(r_id)  # Fix in the simu: temperature forecast given

        # WaterTank: remove the rooms classified as "water tank" from the linear RC model
        for r_id in self.bdms.room_ids_list:
            if self.bdms.room(r_id).type == EMS_ROOM_TYPE_WATER_TANK:

                # The water tank is located in a room: the one it has an interface with
                interf_room = self.bdms.room(r_id).interfaces.keys()
                if len(interf_room) > 0:
                    self.map_watertank_room[r_id] = interf_room[0]

                    # Remove this interface from the list of the other rooms (normally only one !)
                    #TODO: dangerous to directly DEL an interface, even though it's only use here ..
                    for r_other_id in self.bdms.room_ids_list:
                        interf_other_room = self.bdms.room(r_other_id).interfaces
                        if r_id in interf_other_room.keys():
                            del interf_other_room[r_id]
                else:
                    self.map_watertank_room[r_id] = None

        # Loads
        self.list_loads_def = self.bdms.get_entity_list([EMS_CATEGORY_ENTITY_LOAD, EMS_CATEGORY_ENTITY_LOAD_DEFERRABLE])

        # Init the load scheduler
        self.load_scheduler.initFromEMS(self.list_loads_def)

        # --- Thermal loads
        list_alltherm_loads = self.bdms.get_entity_list([EMS_CATEGORY_ENTITY_LOAD,
                                                                  EMS_CATEGORY_ENTITY_LOAD_THERM])

        for i in range(len(list_alltherm_loads)):
            (l_id, l_obj) = list_alltherm_loads[i]
            linked_room_id = self.get_thermal_load_room(l_id)
            if self.bdms.room(linked_room_id).type != EMS_ROOM_TYPE_WATER_TANK:
                self.list_space_hvac.append(list_alltherm_loads[i])
            else:
                self.list_waterheater.append(list_alltherm_loads[i])

        # RC thermal simulation
        self.nb_interf_nodes = self.get_nb_interface_nodes(self.list_room_active_simu)

        # Batteries
        self.list_battery = self.bdms.get_entity_list([EMS_CATEGORY_ENTITY_STORAGE])

    ###########
    # OPTIMIZATION CONSTRAINTS: COMFORT AND PHYSICAL CONSTRAINTS
    ###########

    def get_hvac_constraints(self, p_space_hvac):
        c = []

        for i in range(len(self.list_space_hvac)):
            (therm_load_id, therm_load_obj) = self.list_space_hvac[i]
            c += [p_space_hvac[i, :] <= therm_load_obj.max_power]
            c += [p_space_hvac[i, :] >= 0]

        return c

    def get_battery_constraints(self, energy_storage, p_storage):
        c = []

        for i in range(len(self.list_battery)):
            (batt_id, batt_obj) = self.list_battery[i]
            cur_soc = batt_obj.current_soc
            max_power_ch = batt_obj.max_power_charge(cur_soc)
            min_power_disch = batt_obj.min_power_discharge(cur_soc)

            c += [p_storage[i, :] <= max_power_ch]
            c += [p_storage[i, :] >= min_power_disch]

            capa = batt_obj.capacity
            c += [energy_storage[i, :] <= batt_obj.max_soc * capa]
            c += [energy_storage[i, :] >= batt_obj.min_soc * capa]
            c += [energy_storage[i, 0] == (cur_soc * capa)]

        return c

    def get_generation_constraints(self, generation_sold, gen_power_pred):
        c = []

        for t in range(len(gen_power_pred)):
            c += [generation_sold[0, t] <= 0]
            c += [generation_sold[0, t] >= gen_power_pred[t]]

        return c

    def get_roomtemp_constraints(self, t_pack, temp_node, eps_temp):
        (t_cur, t_hor, t_step) = t_pack
        nb_pts_T = int(t_hor/t_step)
        c = []

        for t in range(1, nb_pts_T+1):
            t_abs = t_cur + t * t_step

            # ROOM TEMPERATURE
            i = 0
            for r_id in self.list_room_active_simu:
                temp_constr = self.bdms.room(r_id).get_comfort_constraint(EMS_COMFORT_temperature)

                if temp_constr is not None:
                    c += [temp_node[i, t] <= temp_constr.get_max(t_abs) + eps_temp[i, t-1]]
                    c += [temp_node[i, t] >= temp_constr.get_min(t_abs) - eps_temp[i, t-1]]
                    c += [eps_temp[i, t-1] >= 0]
                i += 1

        return c

    def get_watertank_constraints(self, p_waterheater):
        c = []

        for i in range(len(self.map_watertank_room.keys())):
            (therm_load_id, therm_load_obj) = self.list_waterheater[i]
            c += [p_waterheater[i, :] <= therm_load_obj.max_power]
            c += [p_waterheater[i, :] >= 0]

        return c

    def get_watertanktemp_constraints(self, t_pack, watertank_temp, eps_watertank_temp):
        (t_cur, t_hor, t_step) = t_pack
        nb_pts_T = int(t_hor / t_step)
        c = []

        for t in range(1, nb_pts_T + 1):
            t_abs = t_cur + t * t_step

            # WATER TANK TEMPERATURE
            i = 0
            for wt_id in self.map_watertank_room.keys():
                wt_temp_constr = self.bdms.room(wt_id).get_comfort_constraint(EMS_COMFORT_temperature)

                if wt_temp_constr is not None:
                    c += [watertank_temp[i, t] <= wt_temp_constr.get_max(t_abs) + eps_watertank_temp[i, t - 1]]
                    c += [watertank_temp[i, t] >= wt_temp_constr.get_min(t_abs) - eps_watertank_temp[i, t - 1]]
                    c += [eps_watertank_temp[i, t - 1] >= 0]
                i += 1

        return c

    ###########
    # OPTIMIZATION CONSTRAINTS: MODEL EVOLUTION
    ###########

    def get_rc_thermal_model_constr(self, t_pack, temp_node, p_space_hvac, weather_pred):

        MPC_THERMAL_MODEL_INIT_TEMP = 20  # TODO: use current temperature and assess hidden states

        # Time data
        (t_cur, t_hor, t_step) = t_pack
        nb_pts_T = int(t_hor / t_step)

        # Weather data
        (temp_pred, irr_pred) = weather_pred

        c = []

        nb_active_rooms = len(self.list_room_active_simu)
        nb_fix_rooms = len(self.list_room_fix_simu)
        size_x = nb_active_rooms + self.nb_interf_nodes  # #room + #nodes-interface

        temp_0_var = size_x * [MPC_THERMAL_MODEL_INIT_TEMP]

        # Initial condition of the thermal model
        idx = 0
        for r_id in self.list_room_active_simu:  # Room temperature points
            temp_0_var[idx] = self.bdms.room(r_id).get_comfort_value(EMS_COMFORT_temperature)
            idx += 1

        # Interface temperature hidden states
        for i in range(self.nb_interf_nodes):
            temp_0_var[nb_active_rooms + i] = MPC_THERMAL_MODEL_INIT_TEMP #TODO: assess hidden states

        # Forecast temperature points for the horizon N
        temp_fix_forecast = list()
        for r_id in self.list_room_fix_simu:
            temp_fix_forecast.append(self.bdms.room(r_id).get_forecast(EMS_COMFORT_temperature, t_pack))

        # --- Heat gain due to the sun, water tank and internal loads

        # -- Sun heat gain, per room
        heat_gain_vect = []
        idx_r = 0
        for r_id in self.list_room_active_simu:
            tot_heat_distrurb = nb_pts_T * [0.0]

            # SUN disturbance
            if self.bdms.room(r_id).has_interface_with(neighbor=EMS_OUTSIDE_ROOM_NAME,
                                                       type_interface=EMS_INTERFACE_WINDOW):
                sun_heat_disturb = self.bdms.room(r_id).get_solar_heat_gain(t_pack, irr_pred)
                tot_heat_distrurb = [x + y for (x, y) in zip(tot_heat_distrurb, sun_heat_disturb)]

            # Water Tank effects
            for wt_id, wt_connectedroom_id in self.map_watertank_room.items():
                if r_id == wt_connectedroom_id:
                    u_value = self.get_uvalue_watertank(wt_id, r_id)
                    t_wt_mean = self.get_room_meantemp(wt_id, t_cur)
                    t_room_mean = self.get_room_meantemp(r_id, t_cur)
                    watertank_heat_disturb = u_value * (t_wt_mean - t_room_mean)
                    for i in range(nb_pts_T):
                        tot_heat_distrurb[i] += watertank_heat_disturb

            heat_gain_vect.append(tot_heat_distrurb)  # Total internal gain for this room

        # Load the initial temperature values from the measurements: HUMAN SPACES
        c += [temp_node[:, 0] == temp_0_var]

        # EQUATIONS
        for t in range(0, nb_pts_T):
            t_abs = t_cur + t * t_step

            # --- Thermal model equations

            # Room temp unknown, room temp forecast and interface node temp
            ig = list(heat_gain_vect[i][t] for i in range(len(heat_gain_vect)))  # sun disturbance
            fix_temp = list(temp_fix_forecast[i][t] for i in range(len(temp_fix_forecast)))  # forecast temp. influence

            # Thermal simulation RC matrices (Heat gain due to the HVACs: DEPENDS ON t !)
            discr_sys = self.get_thermal_matrix(t_step=t_step,
                                                t_fix_forecast=temp_pred[t])

            rc_thermal_matrix_a, b_u, (rc_thermal_matrix_b_d, rc_thermal_matrix_b_ig) = discr_sys
            # Term linked to HVAC
            if b_u.size > 0:
                u_term = b_u * p_space_hvac[:, t]
            else:
                u_term = np.zeros((size_x, 1))

            # Term linked to disturbances: fixed temperature and internal gain
            disturb_term = np.matmul(rc_thermal_matrix_b_d, fix_temp)  # forecast temp
            disturb_term += np.matmul(rc_thermal_matrix_b_ig, ig)  # forecast heat gain

            # FINALLY model the evolution of the expected space temperatures
            c += [temp_node[:, t + 1] == rc_thermal_matrix_a * temp_node[:, t] + u_term + disturb_term.transpose()]

        return c

    def get_watertank_model_constr(self, t_pack, watertank_temp, p_waterheater):

        # TODO: move it out of this function
        MPC_WATERTANK_MODEL_T_INLET = 12  # inlet water temperature [deg celcius]
        MPC_THERMAL_MODEL_INIT_TEMP_WATER = 50

        # Time data
        (t_cur, t_hor, t_step) = t_pack
        nb_pts_T = int(t_hor / t_step)

        c = []
        # Load the initial temperature values from the measurements: WATER TANKS

        watertank_temp_0_var = len(self.map_watertank_room) * [MPC_THERMAL_MODEL_INIT_TEMP_WATER]

        # Initial condition of the thermal model
        idx = 0
        for wt_id in self.map_watertank_room.keys():  # Room temperature points
            watertank_temp_0_var[idx] = self.bdms.room(wt_id).get_comfort_value(EMS_COMFORT_temperature)
            idx += 1

        if len(self.map_watertank_room.keys()) > 0:
            c += [watertank_temp[:, 0] == watertank_temp_0_var]

        # Disturbance prediction, water draw
        watertank_hotwater_draw = []
        list_wt_ids = self.map_watertank_room.keys()
        for i in range(len(list_wt_ids)):
            r_id = list_wt_ids[i]

            # The water opener data structure
            hwt_opener = self.bdms.room(r_id).get_actuators(ACTUATOR_TYPE_ACTION_DIMMER, None)
            opener_model = hwt_opener[0].model_param
            max_draw = 1
            if VE_VOPENER_VALUE_WHEN_FULLY_OPEN in opener_model:
                max_draw = opener_model[VE_VOPENER_VALUE_WHEN_FULLY_OPEN]
                forecast_wt_status = hwt_opener[0].get_forecast(t_pack)
                watertank_hotwater_draw.append([max_draw*x/100.0 for x in forecast_wt_status])

        # EQUATIONS
        for t in range(0, nb_pts_T):
            t_abs = t_cur + t * t_step

            # -- Water tanks

            for i in range(len(list_wt_ids)):
                r_id = self.map_watertank_room[list_wt_ids[i]]
                t_amb = self.get_room_meantemp(r_id)

                wt_mat_a, wt_mat_bu, wt_disturb = self.get_watertank_model(t_step, (list_wt_ids[i], r_id), (
                watertank_hotwater_draw[i][t], t_amb, MPC_WATERTANK_MODEL_T_INLET))
                c += [watertank_temp[i, t + 1] == wt_mat_a * watertank_temp[i, t] + wt_mat_bu * p_waterheater[i, t] + wt_disturb]

        return c

    def get_battery_model_constr(self, t_pack, energy_storage, p_storage):

        # Time data
        (t_cur, t_hor, t_step) = t_pack
        nb_pts_T = int(t_hor / t_step)

        c = []

        # --- Batteries energy equations
        for t in range(0, nb_pts_T):
            for i in range(len(self.list_battery)):
                (bat_id, bat_obj) = self.list_battery[i]
                c += [energy_storage[i, t + 1] == (1 - bat_obj.leak_coeff) * energy_storage[i, t] + bat_obj.efficiency * t_step / 3600.0 * p_storage[i, t]]

        return c

    ###########
    # MODEL MATRICES
    ###########

    def get_thermal_hvac_mat(self, size_x, time_instant, t_fix_forecast=None):
        """
        Compute the matrix that links the effect of HVAC to each room
        :param size_x: the size of the RC-matrix for room thermal simulation
        :param list_hvac_room: a tuple (x, y) where:
         - x is a list of tuple (id, obj) representing the space heater
         - y is an ordered list that link each room to the right row in the RC-matrix
        :param t_fix_forecast: the fix temperature forecast
        :return: an array of size (size_x, #HVAC) containing the coefficients of the diff equations
        """
        bb = np.zeros((size_x, len(self.list_space_hvac)))

        idx_th = 0
        for (l_id, l_obj) in self.list_space_hvac:
            r_id = self.get_thermal_load_room(l_id)
            if r_id is None or r_id not in self.list_room_active_simu:  # Only the HVACs in simulated room make sense
                continue

            # Put the right variable in the vector "u"
            idx_r = self.list_room_active_simu.index(r_id)

            # Get the thermal efficiency (elec -> therm heat) from the load object and place it at the right place
            if l_obj.device_type == EMS_LOAD_DEVICE_TYPE_HP and t_fix_forecast is not None:  # need t_amb and t_ws
                # Calculate the mean temperature of the supply zone
                t_sup_mean = self.get_room_meantemp(r_id, time_instant)
                efficiency_load = l_obj.thermal_efficiency((t_fix_forecast, t_sup_mean))
            else:
                efficiency_load = l_obj.thermal_efficiency  # otherwise, fixed value

            bb[idx_r][idx_th] = efficiency_load / self.bdms.room(r_id).thermal_capa

            idx_th += 1

        return bb

    def get_thermal_matrix(self, t_step, t_fix_forecast):
        """
        Compute the discretized RC-matrices A_discr and B_discr of the system:

        d(x)/dt = A * x + B_u * u + B_d * d
        (y = C x) C = [0, ..., 0]

        x_(t+1) = A_discr * x(t) + B_discr * [u ; d]

        :param t_step: the discretization step (in seconds)
        :param t_fix_forecast: the forecast external temperature for COP calculation TODO: MOVE IT INSIDE THE FUNCTION
        :return: Two array, A_discr and B_discr, that represent the discretized thermal RC-matrices
        """

        OPEN_INTERFACE_R = 0.001  # TODO: move it out of this function ..

        nb_active_rooms = len(self.list_room_active_simu)  # amount of active rooms in the simulation
        size_x = self.nb_interf_nodes + nb_active_rooms  # amount of rows in the matrix A

        # RC matrix (continuous) for inner state update
        mat_state = np.zeros((size_x, size_x))

        # RC matrix (continuous) that describes the impact of each external fixed temperature (FORECAST)
        mat_fix_tmp = np.zeros((size_x, len(self.list_room_fix_simu)), dtype=np.float)

        # For matrix creation purpose: link 2 rooms to their interface
        room_link_already_visited = dict()
        for r_id in self.list_room_active_simu:
            room_link_already_visited[r_id] = list()

        ################## STEP 1 ######################
        ######### Continuous matrices creation #########
        ################################################

        k = nb_active_rooms  # the index pointing to the right interface temperature node
        for i in range(nb_active_rooms):
            r_id = self.list_room_active_simu[i]
            c_room = self.bdms.room(r_id).thermal_capa

            # Loop over all the interfaces this room has with other rooms
            list_room_interface = self.bdms.room(r_id).interfaces
            for n_id in list_room_interface.keys():

                # This interface has already been handled ?
                if n_id in self.list_room_active_simu and r_id in room_link_already_visited[n_id]:
                    continue
                else:
                    room_link_already_visited[r_id].append(n_id)

                # The general case is to consider multiple interface for each neighbor
                interf_list = list_room_interface[n_id]
                if type(interf_list) is not list:
                    interf_list = [interf_list]

                # Go through all the interface objects this room r_id has with the neighbor room n_id
                for i_obj in interf_list:

                    # RC parameters
                    rc_model = i_obj.thermalProp
                    r_list = rc_model.resistiveParameters
                    c_list = rc_model.capacitiveParam
                    if i_obj.isOpen:  #TODO: Not correct: cannot take the current value for the future ...
                        r_list = [OPEN_INTERFACE_R]
                        c_list = []

                    # Normal case: multiple intermediate temperature points
                    if len(c_list) > 0:

                        # ROOM r_id
                        coeff_room = 1 / (r_list[0] * c_room)
                        mat_state[i][i] -= coeff_room
                        mat_state[i][k] += coeff_room

                        # ROOM n_id
                        if n_id in self.list_room_active_simu:
                            idx_n = self.list_room_active_simu.index(n_id)
                            dk = rc_model.orderModel - 1
                            coeff_room = 1 / (r_list[dk] * self.bdms.room(n_id).thermal_capa)
                            mat_state[idx_n][idx_n] -= coeff_room
                            mat_state[idx_n][k + dk] += coeff_room

                        for j in range(rc_model.orderModel):
                            idx1 = k - 1
                            idx2 = k + 1

                            # Update the influence of room n_id temperature
                            if j == 0:
                                idx1 = i

                            mat_state[k][k] -= (1.0 / (r_list[j] * c_list[j]) + 1.0 / (r_list[j + 1] * c_list[j]))
                            mat_state[k][idx1] = 1.0 / (r_list[j] * c_list[j])

                            # Update the influence of room n_id temperature
                            if j == rc_model.orderModel - 1:
                                if n_id in self.list_room_active_simu:
                                    idx2 = self.list_room_active_simu.index(n_id)
                                else:
                                    idx2 = self.list_room_fix_simu.index(n_id)

                            if n_id in self.list_room_fix_simu and j == rc_model.orderModel - 1:  # Take the fixed temperature in this case
                                mat_fix_tmp[k][idx2] = 1.0 / (r_list[j + 1] * c_list[j])
                            else:
                                mat_state[k][idx2] = 1.0 / (r_list[j + 1] * c_list[j])

                            k += 1  # INCREMENT THE INDEX OF THE INTERFACE TEMPERATURE POINTS

                    else:  # Particular case: direct link between two rooms via a resistance
                        r = r_list[0]
                        coeff1 = 1.0 / (r * c_room)
                        mat_state[i][i] -= coeff1  # neighbor effect on r_id
                        if n_id in self.list_room_active_simu:  # Interaction with another temperature point to simulate
                            coeff2 = 1.0 / (r * self.bdms.room(n_id).thermal_capa)
                            n_index = self.list_room_active_simu.index(n_id)

                            mat_state[i][n_index] += coeff1  # neighbor effect on r_id
                            mat_state[n_index][n_index] -= coeff2  # effect of r_id on the neighbor
                            mat_state[n_index][i] += coeff2  # effect of r_id on the neighbor
                        else:  # Interaction with a fixed temperature: don't
                            n_fix_index = self.list_room_fix_simu.index(n_id)
                            mat_fix_tmp[i][n_fix_index] += coeff1

        # The matrix that links the HVAC power to its effect on SPACES
        mat_fix_hvac = self.get_thermal_hvac_mat(size_x, t_fix_forecast)

        # The matrix that links the SUN HEAT GAIN to its effect on SPACES
        mat_fix_internalheat = np.zeros((size_x, len(self.list_room_active_simu)), dtype=np.float)
        for i in range(len(self.list_room_active_simu)):
            r_id = self.list_room_active_simu[i]
            c_room = self.bdms.room(r_id).thermal_capa
            mat_fix_internalheat[i, i] = 1 / c_room

        #################### STEP 2 ########################
        ######### Discretize the continuous system #########
        ####################################################

        # From continuous model to discretized one:
        mat_b_cont = np.concatenate((mat_fix_tmp, mat_fix_internalheat, mat_fix_hvac), axis=1)

        c_mat = nb_active_rooms * [1] + (size_x - nb_active_rooms) * [0]
        d_mat = (mat_b_cont.size / size_x) * [0]

        thermal_sys = control.ss(mat_state, mat_b_cont, c_mat, d_mat)
        thermal_sys_discr = control.sample_system(thermal_sys, t_step)
        rc_mat_a_dt, rc_mat_b_dt, c, d = control.ssdata(thermal_sys_discr)

        # Separate B_u and B_d
        aa_d = np.array(rc_mat_a_dt)

        bb_d = rc_mat_b_dt[:, 0:len(self.list_room_fix_simu)]  # RC param for fixed temp effect
        bb_ig = rc_mat_b_dt[:, len(self.list_room_fix_simu):len(self.list_room_fix_simu) + len(
            self.list_room_active_simu)]  # RC param for internal gain heat effect
        bb_u = rc_mat_b_dt[:,
               len(self.list_room_fix_simu) + len(self.list_room_active_simu):]  # RC param for HVAC heat effect

        return aa_d, bb_u, (bb_d, bb_ig)

    def get_watertank_model(self, t_step, map_room_id, fixed_env_values):

        """
        Return the linear water tank model:
        t <- t + TODO
        :param t_step:
        :param map_room_id:
        :param fixed_env_values:
        :return: [A, B, D]
        """

        # Get tank details
        wt_id, ambient_room_id = map_room_id
        room_obj = self.bdms.room(wt_id)

        # Fixed value
        water_draw, amb_temp, inlet_temp = fixed_env_values

        # Compute coefficients
        ewh_capacitance = room_obj.volume * RHO_WATER_STC * WATER_HEAT_CAPA
        ewh_b = water_draw * WATER_HEAT_CAPA
        ewh_g = self.get_uvalue_watertank(wt_id, ambient_room_id)

        r_bis = 1 / (ewh_g + ewh_b)
        dt_term = math.exp(-t_step / (r_bis * ewh_capacitance))

        # The coeff of current tank temperature
        a_mat = dt_term

        # The coeff of the electrical power
        load_id = room_obj.get_comfort_loads(EMS_COMFORT_temperature)
        load_obj = self.bdms.get_energy_entity_obj(EMS_CATEGORY_ENTITY_LOAD, load_id[0])

        b_u = (1 - dt_term) * r_bis * load_obj.thermal_efficiency

        # The coeff of ambient temperature and inlet temperature
        d_disturb = (1 - dt_term) * r_bis * (ewh_g * amb_temp + ewh_b * inlet_temp)

        return a_mat, b_u, d_disturb

    def get_thermal_load_room(self, th_l_id):
        """
        This method returns the room that contains the load "th_l_id"
        :param th_l_id:
        :return:
        """

        for r_id in self.bdms.room_ids_list:
            if th_l_id in self.bdms.room(r_id).get_comfort_loads(EMS_COMFORT_temperature):
                return r_id

        return None

    def get_nb_interface_nodes(self, active_rooms):
        """
        Return the amount of unseen model variables used for RC zone temperature model
        :param active_rooms: the list of active space
        :return: an integer
        """
        x = 0
        already_visited = list()

        for r_id in active_rooms:
            interf = self.bdms.room(r_id).interfaces
            for n_id in interf.keys():

                # Skip this link if n_id -> r_id has already been taken into account
                if n_id in already_visited:
                    continue

                i_obj = interf[n_id]

                if type(i_obj) is list:
                    for ii_obj in i_obj:
                        x += ii_obj.thermalProp.orderModel
                else:
                    x += i_obj.thermalProp.orderModel

            already_visited.append(r_id)

        return x

    def get_room_meantemp(self, r_id, time_instant):
        """
        Return the mean expected value of the temperature of a room.
        (1) if the reference is provided, it is assumed to be tracked and
        (2) if the max and min temperature and given, return the mean between those two
        (3) else, return the current value ..
        :param r_id:
        :param time_instant:
        :return:
        """

        room_obj = self.bdms.room(r_id)

        # Is there a reference to track?
        ref_value = room_obj.get_reference_value(EMS_COMFORT_temperature)
        if ref_value is not None:
            return ref_value

        # If not, take the mean of the bounds
        constr = room_obj.get_comfort_constraint(EMS_COMFORT_temperature)
        if constr is not None:
            return (constr.get_max(time_instant)+constr.get_min(time_instant))/2

        # Otherwise, return the current value ..
        return room_obj.get_comfort_value(EMS_COMFORT_temperature)

    def get_uvalue_watertank(self, wt_id, r_id=None):
        """
        Compute the u-value of the water tank
        :param wt_id: the water tank space
        :param r_id: the connected room space
        :return: a float
        """

        room_obj = self.bdms.room(wt_id)

        # Normally these is only 1 interface connected to the water tank
        if r_id is None:
            interface_list = room_obj.interfaces
            list_rooms = interface_list.keys()
            interface_obj = interface_list[list_rooms[0]][0]
        else:
            interface_obj = room_obj.interfaces[r_id][0]

        u_value = interface_obj.thermalProp.resistiveParameters[0]

        return u_value
